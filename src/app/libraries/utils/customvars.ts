


export interface AdviableVar {
	adviable: boolean;
}


export interface ActionsVars {
	is_new: boolean;
	updated: boolean;
	deleted: boolean;
	exists_slug:AdviableVar;
	exists_title:AdviableVar;
}

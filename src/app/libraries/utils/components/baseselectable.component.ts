 import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Globals } from "../../../system/components/main/globals";
import { BaseSaveComponent } from './basesave.component';
import { BaseListComponent } from './baselist.component';
import { Data,Pagination } from './attributes';
import { CustomMethodsService,applyMixins } from '../../../libraries/utils/custommethods.service';
import { AfterViewInit, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';

export class BaseListSelectableComponent
{


    items: Data[];
    query:string;
    selected:boolean;
    pages: number;
    edit_url:string;
    new_url:string;
    list_url:string;
    selectedable:boolean = false;
    model: any[];
    modelChange = new EventEmitter<any[]>();

    SELECTABLE_SELECT_ALL()
    {
      if(this.selectedable == false)
      {
          this.selectedable  = true;
          this.items.forEach(obj => {
              obj.selected = true;
          })
         this.modelChange.emit( this.items.filter(objx => objx.selected === true));
     }else
     {
        this.selectedable  = false;
        this.items.forEach(obj => {
          obj.selected = false;
        })
        this.modelChange.emit( this.items.filter(objx => objx.selected === true));
     }
    }
    SELECTABLE_SELECT(item:Data)
    {
      if(item.selected == false)
      {
         item.selected = true;
      }else{
        item.selected = false;
      }
      this.modelChange.emit( this.items.filter(objx => objx.selected === true));
  }
}

export class BaseListItemSelectableComponent implements AfterViewInit {
  page:number;
  items: Data[];
  query:string;
  selected:boolean;
  pages: number;
  model: any[];
  edit_url:string;
  new_url:string;
  list_url:string;
  modelChange = new EventEmitter<any[]>()
  constructor(
    private router: Router, 
    private global:Globals,
    private categories:any,
    private elementRef: ElementRef
    ) { 

  }
  ngAfterViewInit() {
    //inject dashboard
    this.global.initializeActions();

    this.categories.List(this.page).subscribe((data:Pagination) => {
      data.items.forEach(obj => {
          obj.checked = false;
          obj.selected = false;
          this.model.forEach(objx => {
            if (objx.id == obj.id)
            {
              obj.selected = true;
            }
          })
         })
      this.items = data.items ;
      this.pages = data.pages;
    });

  }
}

export class BaseItemSelectableNumberComponent implements AfterViewInit {
  page:number;
  items: Data[];
  query:string;
  selected:boolean;
  pages: number;
  model: number;
  edit_url:string;
  new_url:string;
  list_url:string;
  modelChange = new EventEmitter<number>();
  constructor(
    private router: Router, 
    private global:Globals,
    private categories:any,
    private elementRef: ElementRef
    ) { 
    this.model = 0;
  }
  ngAfterViewInit() {
    //inject dashboard
    this.global.initializeActions();

    this.categories.List(this.page).subscribe((data:Pagination) => {
      data.items.forEach(obj => {
          obj.checked = false;
          obj.selected = false;
          if (this.model == obj.id)
          {
            obj.selected = true;
          }
         })
      this.items = data.items ;
      this.pages = data.pages;
    });

  }
  SELECTABLE_SELECT(item:Data){
    this.items.forEach(obj => {
      obj.checked = false;
      obj.selected = false;
    })
    if(item.selected == false)
    {
      item.selected = true;
    }else{
      item.selected = false;
    }
    this.modelChange.emit( item.id);
  }
}


applyMixins(BaseListItemSelectableComponent, [ BaseListComponent,BaseListSelectableComponent, BaseSaveComponent]);
applyMixins(BaseItemSelectableNumberComponent, [ BaseListComponent, BaseSaveComponent]);


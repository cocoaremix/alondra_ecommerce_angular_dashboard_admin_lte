import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AttributableSelectableComponent } from './attributable.component';

describe('AttributableSelectableComponent', () => {
  let component: AttributableSelectableComponent;
  let fixture: ComponentFixture<AttributableSelectableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AttributableSelectableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttributableSelectableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { BaseService } from '../../../base.service';
import { Router } from '@angular/router';
import { Globals } from "../../../../../../system/components/main/globals";
import { AfterViewInit, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { BaseListItemSelectableComponent } from "../../../baseselectable.component";
import { Data } from '../../../attributes';
@Component({
  selector: 'app-attributable_selectable',
  templateUrl: './attributable.component.html',
  styleUrls: ['./attributable.component.css']
})
export class AttributableSelectableComponent extends BaseListItemSelectableComponent {

  items: Data[];
  @Input() MODULE_NAME:string;
  @Input() model: Data[];
  @Output() modelChange = new EventEmitter<Data[]>();
  @Input() list_url:string;
  @Input() search_url:string;
  @Input() new_url:string;
  @Input() get_url:string;
  @Input() find_url:string;
  @Input() update_url:string;
  @Input() delete_url:string;
  constructor(
      private routerx: Router, 
      private globalx:Globals,
      private categoriesx:BaseService,
      private elementRefx: ElementRef
    ) {

      super(routerx, globalx,categoriesx, elementRefx);
   }

   ngAfterViewInit() {
    //inject dashboard

    this.globalx.initializeActions();
    this.categoriesx.list_url = this.list_url;
    this.categoriesx.search_url = this.search_url;
    this.categoriesx.new_url = this.new_url;
    this.categoriesx.get_url = this.get_url;
    this.categoriesx.find_url = this.find_url;
    this.categoriesx.update_url = this.update_url;
    this.categoriesx.delete_url = this.delete_url;
    this.categoriesx.List(this.page).subscribe((data:Data) => {
      data.items.forEach(obj => {
          obj.checked = false;
          obj.selected = false;
          this.model.forEach(objx => {
            if (objx.id == obj.id)
            {
              obj.selected = true;
            }
          })
         })
      this.items = data.items ;
      this.pages = data.pages;
    });

  }
}

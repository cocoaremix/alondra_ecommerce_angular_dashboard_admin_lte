import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AttributableEditComponent } from './attributable.component';

describe('AttributableEditComponent', () => {
  let component: AttributableEditComponent;
  let fixture: ComponentFixture<AttributableEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AttributableEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttributableEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

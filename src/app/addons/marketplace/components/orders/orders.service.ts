import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, } from '@angular/common/http';
import { Headers } from '@angular/http';
import {RequestOptions, Request, RequestMethod} from '@angular/http';
import { environment } from '../../../../../environments/environment';
import { Globals } from "../../../../system/components/main/globals";
import { CookieService } from 'ngx-cookie-service';
import { HttpJsonHeaderOptionsService } from '../../../../libraries/utils/httpoptions.service';


@Injectable({
  providedIn: 'root'
})
export class OrdersMarketplaceService {

  
  constructor(
    private http: HttpClient, 
    private cookie:CookieService,
    private global:Globals, 
    private header: HttpJsonHeaderOptionsService) { }


  List(page,user_id) {
     
    const postedData = {page:page,user_id:user_id}
    return this.http
      .post( 
        environment.url + "marketplace-my-articles/",
        postedData,
        this.header.get_auth(this.global.token)
      );
  }

  Search(page, query,user_id) {
    const postedData = {
      page:page,
      query:query,
      user_id:user_id
    }
    return this.http
      .post( 
        environment.url + "marketplace-my-articles/search/",
        postedData,
        this.header.get_auth(this.global.token)
      );
  }

  GetSales(id) {
   
    const postedData = {
      pk:id
    }    
    return this.http
      .post( 
        environment.url + "marketplace-my-articles/sales/",
        postedData,
        this.header.get_auth(this.global.token)
      );
  }



}

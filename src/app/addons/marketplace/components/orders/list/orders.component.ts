import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import { OrdersMarketplaceService } from '../../orders/orders.service'
import { MarketplaceItems,PaginationMarketplaceItems } from '.././orders'
import { Globals } from "../../../../../system/components/main/globals";
@Component({
  selector: 'app-marketplace-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersMarketplaceComponent implements OnInit {
  page:number;
  items: MarketplaceItems[];
  query:string;
  selected:boolean;
  pages: number;
  constructor(
    private router: Router, 
    private route:ActivatedRoute, 
    private categories:OrdersMarketplaceService,
    private global:Globals
    ) { }
  ngOnInit() 
  {
    //inject dashboard
    this.global.initializeActions();
    this.page = 1;
    this.query = '';
    this.selected = false;
    this.items = [];
    this.route.params.subscribe(params =>{
      if (typeof params['id'] !== 'undefined') 
      { 
        this.categories.List(this.page,params['id']).subscribe((data:PaginationMarketplaceItems) => {
          data.items.forEach(obj => {
                obj.checked = false;
                obj.selected = false;
                /*this.categories.GetSales(obj.id).subscribe((datax:any) => {
                    obj.sales = datax.sales;
                    console.log(obj.sales)
                  });*/

             })
           this.items = data.items ;
           this.pages = data.pages;
        });
      }
    });
    
    
  }
  loadmore()
  {
      this.page=this.page +1;

      if (this.page<this.pages+1)
      {
        if(this.query.length >0 )
        {
          this.route.params.subscribe(params =>{
            if (typeof params['id'] !== 'undefined') 
            { 
            
              this.categories.Search(
                this.page,
                this.query,
                params['id']
              ).subscribe((data:PaginationMarketplaceItems) => {
                data.items.forEach(obj => {
                    obj.checked = false;
                    obj.selected = false;
                })
                 
                this.items.push(...data.items);
                 
                this.pages = data.pages;
                this.incrementpage();
              });
            }else
            {
              this.categories.List(this.page,params['id']).subscribe((data:PaginationMarketplaceItems) => {
                this.pages = data.pages;
                data.items.forEach(obj => {
                    obj.checked = false;
                    obj.selected = false;
                 })
                this.items.push(...data.items);
                this.incrementpage();
              });
            }
          
          });
        }
       
       
     }
 
   
  }
  
  incrementpage()
  {
    if( this.page > this.pages)
    {
      this.page = this.pages;
    }
  }

  GetSales(id)
  {
    var sales = 0;
    
    return sales;
    
  }

 
 
  find()
  {
  
    this.page = 1;
    this.route.params.subscribe(params =>
    {
      if (typeof params['id'] !== 'undefined') 
      {

        if(this.query.length > 0 )
        {
            this.categories.Search(
              this.page,
              this.query,
              params['id']
            ).subscribe((data:PaginationMarketplaceItems) => {

             this.items = data.items ;
              this.pages = data.pages;
          });
        }else
        {
          this.categories.List(this.page,params['id']).subscribe((data:PaginationMarketplaceItems) => {

            this.items = data.items ;
             this.pages = data.pages;
          });
        }
      }

    })
    
  }
}

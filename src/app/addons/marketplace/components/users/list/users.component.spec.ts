import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersMarketplaceComponent } from './users.component';

describe('UsersMarketplaceComponent', () => {
  let component: UsersMarketplaceComponent;
  let fixture: ComponentFixture<UsersMarketplaceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersMarketplaceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersMarketplaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});


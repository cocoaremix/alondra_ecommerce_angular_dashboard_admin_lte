import { Component, OnInit } from '@angular/core';
import { UsersService } from '../../users/users.service'
import { Data,Pagination } from '.././users'
import { Router } from '@angular/router';
import { Globals } from "../../../../../system/components/main/globals";

@Component({
  selector: 'app-marketplace-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersMarketplaceComponent implements OnInit {

  page:number;
  items: Data[];
  query:string;
  selected:boolean;
  pages: number;
  constructor(
    private router: Router, 
    private posts:UsersService,
    private global:Globals
    ) { }
  ngOnInit() {
    //inject dashboard
    this.global.initializeActions();
    this.page = 1;
    this.query = '';
    this.selected = false;
    this.posts.List(this.page).subscribe((data:Pagination) => {
      data.items.forEach(obj => {
            obj.checked = false;
         })
       this.items = data.items ;
       this.pages = data.pages;
    });
    
  }
  loadmore()
  {
    this.page ++;
    if( this.page >= this.pages)
    {
      this.page = this.pages;
    }
    if(this.query.length >0 )
    {
        this.posts.Search(
          this.page,
          this.query
        ).subscribe((data:Pagination) => {
          data.items.forEach(obj => {
            obj.checked = false;
         })
        this.items.concat(data.items) ;
        this.pages = data.pages;
      });
    }else
    {
      this.posts.List(this.page).subscribe((data:Pagination) => {
        this.pages = data.pages;
        data.items.forEach(obj => {
            obj.checked = false;
         })
        this.items.concat(data.items) ;
      });
    }
   
  }
  SELECT_ALL(){
    if(this.selected == false)
    {
      
     this.items.forEach(obj => {
            obj.checked = true;
         })
   }else
   {

     this.items.forEach(obj => {
            obj.checked = false;
         })
   }
      
  }

  EDIT(item:Data){
    this.router.navigate(['marketplace/user-orders/'+item.id+'/list' ]);
  }
 
  New(){
     this.router.navigate(['/users/new' ]);
  }
  PERMISIONS(item:Data)
  {
    this.router.navigate(['/user/permisions-groups/',item.id ]);
  }

                            
  find()
  {
 
  
    this.page = 1;

    if(this.query.length > 0 )
    {
        this.posts.Search(
          this.page,
          this.query
        ).subscribe((data:Pagination) => {

         this.items = data.items ;
          this.pages = data.pages;
      });
    }else
    {
      this.posts.List(this.page).subscribe((data:Pagination) => {

        this.items = data.items ;
         this.pages = data.pages;
      });
    }
  }
}

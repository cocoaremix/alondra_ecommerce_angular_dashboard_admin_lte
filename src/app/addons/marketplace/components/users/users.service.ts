import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, } from '@angular/common/http';
import { Headers } from '@angular/http';
import { RequestOptions, Request, RequestMethod} from '@angular/http';
import { environment } from '../../../../../environments/environment';
import { Globals } from "../../../../system/components/main/globals";
import { CookieService } from 'ngx-cookie-service';
import { HttpJsonHeaderOptionsService } from '../../../../libraries/utils/httpoptions.service';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})


export class UsersService {

  

  
  constructor(
    private http: HttpClient, 
    private cookie:CookieService,
    private global:Globals, 
    private header: HttpJsonHeaderOptionsService) { }


  me() {
    return this.http
      .get( 
        environment.url + "me/",
         this.header.get_auth(this.global.token)
        
      );
  }

  List(page) {
    const postedData = {page:page}
    return this.http
      .post( 
        environment.url + "users/",
        postedData,
        this.header.get_auth(this.global.token)
      );
  }

  Search(page, query) {
    const postedData = {
      page:page,
      query:query     
    }
    return this.http
      .post( 
        environment.url + "users/search/",
        postedData,
         this.header.get_auth(this.global.token)
      );
  }

  

}

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, } from '@angular/common/http';
import { Headers } from '@angular/http';
import {RequestOptions, Request, RequestMethod} from '@angular/http';
import { environment } from '../../../../../../environments/environment';
import { Globals } from "../../../../../system/components/main/globals";
import { CookieService } from 'ngx-cookie-service';
import { HttpJsonHeaderOptionsService } from '../../../../../libraries/utils/httpoptions.service';
import { BaseService } from "../../../../../libraries/utils/components/base.service";

@Injectable({
  providedIn: 'root'
})
export class FaqCategoryService  extends BaseService {

   constructor(
    private httpx: HttpClient, 
    private cookiex:CookieService,
    private globalx:Globals, 
    private headerx: HttpJsonHeaderOptionsService) { 
      super(httpx,cookiex, globalx, headerx);
      this.list_url="faq-categories/";
      this.search_url="faq-categories/search/";
      this.new_url="faq-category/";
      this.get_url="faq-category/details/";
      this.find_url="faq-category/find/";
      this.find_slug_url="faq-category/find/slug/";
      this.update_url="faq-category/";
      this.delete_url="faq-category/";
  }
 
}

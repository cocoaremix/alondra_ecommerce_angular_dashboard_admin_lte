import { Component, OnInit } from '@angular/core';
import { FaqCategoryService } from '../../categories/category.service'
import { Data,Pagination } from '.././category'
import { Router } from '@angular/router';
import { Globals } from "../../../../../../system/components/main/globals";
import { AfterViewInit, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { BaseListItemSelectableComponent } from "../../../../../../libraries/utils/components/baseselectable.component";

@Component({
  selector: 'app-categories_selectable',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class SupportFaqCategoriesSelectableComponent extends BaseListItemSelectableComponent {

  items: Data[];
  @Input() model: Data[];
  @Output() modelChange = new EventEmitter<Data[]>();
  constructor(
      private routerx: Router, 
      private globalx:Globals,
      private categoriesx:FaqCategoryService,
      private elementRefx: ElementRef
    ) {

      super(routerx, globalx,categoriesx, elementRefx);
      this.edit_url = 'support/faq/categories/edit/';
      this.new_url ='support/faq/categories/new';
   }

 
}

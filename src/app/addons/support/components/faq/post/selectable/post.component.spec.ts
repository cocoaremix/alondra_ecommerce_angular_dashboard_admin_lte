import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupporFaqSelectableComponent } from './post.component';

describe('SupporFaqSelectableComponent', () => {
  let component: SupporFaqSelectableComponent;
  let fixture: ComponentFixture<SupporFaqSelectableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupporFaqSelectableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupporFaqSelectableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

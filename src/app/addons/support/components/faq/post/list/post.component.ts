import { Component, OnInit } from '@angular/core';
import { SupporFaqService } from '../../post/post.service'
import { Data,Pagination } from '.././post'
import { Router } from '@angular/router';
import { Globals } from "../../../../../../system/components/main/globals";
import { BaseListItemsComponent } from "../../../../../../libraries/utils/components/baselist.component";

@Component({
  selector: 'app-support_faq',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class SupportFaqComponent extends BaseListItemsComponent  {
    page:number;
    items: Data[];
    query:string;
    selected:boolean;
    pages: number;
    constructor(
      private  routerx: Router, 
      private globalx:Globals,
      private categoriesx:SupporFaqService
    ) { 
      super(routerx,globalx,categoriesx);
      this.new_url='support/faq/new/';
      this.edit_url ='support/faq/edit/';
   }
 
}

import { Component, OnInit } from '@angular/core';
import { TaxesService } from '../../taxes/taxes.service'
import { Data,Pagination } from '.././taxes'
import { Router } from '@angular/router';
import { Globals } from "../../../../../system/components/main/globals";
import { AfterViewInit, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { BaseListItemSelectableComponent } from "../../../../../libraries/utils/components/baseselectable.component";

@Component({

  selector: 'app-taxes_selectable',
  templateUrl: './taxes.component.html',
  styleUrls: ['./taxes.component.css']
})
export class TaxesSelectableComponent extends BaseListItemSelectableComponent {
  page:number = 1;
  items: Data[];
  query:string = "";
  selected:boolean = false;
  selectedable:boolean = false;
  pages: number = 0;
  @Input() model: Data[];
  @Output() modelChange = new EventEmitter<Data[]>()
  constructor(
    private routerx: Router, 
    private globalx:Globals,
    private categoriesx:TaxesService,
    private elementRefx: ElementRef,

    ) {
     super(routerx,globalx,categoriesx,elementRefx);
  }


}

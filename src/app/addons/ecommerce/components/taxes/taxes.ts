


export interface Data {
	id: number;
	city: string;
	country: string;
	percent: number;
	checked: boolean;
	selected: boolean;
}


export interface Pagination {
	pages: number;
	items: Data[];
	next_page: string;
	prev_page: string;
}
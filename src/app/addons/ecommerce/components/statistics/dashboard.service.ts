import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, } from '@angular/common/http';
import { Headers } from '@angular/http';
import { RequestOptions, Request, RequestMethod} from '@angular/http';
import { environment } from '../../../../../environments/environment';
import { Globals } from "../../../../system/components/main/globals";
import { CookieService } from 'ngx-cookie-service';
import { HttpJsonHeaderOptionsService } from '../../../../libraries/utils/httpoptions.service';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  
  constructor(
    private http: HttpClient, 
    private cookie:CookieService,
    private global:Globals, 
    private header: HttpJsonHeaderOptionsService) { }

  RecentOrders() {
     
    const postedData = {}
    return this.http
      .post( 
        environment.url + "orders/recent/",
        postedData,
        this.header.get_auth(this.global.token)
      );
  }

  TopSelledProducts() {
    const postedData = {}
    return this.http
      .post( 
        environment.url + "top/selled/products/",
        postedData,
        this.header.get_auth(this.global.token)
      );
  }

  TopSalesYear() {
    const postedData = {}
    return this.http
      .post( 
        environment.url + "top/sales/year/",
        postedData,
        this.header.get_auth(this.global.token)
      );
  }

}

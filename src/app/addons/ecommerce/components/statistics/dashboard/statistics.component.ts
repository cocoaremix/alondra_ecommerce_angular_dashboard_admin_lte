import { Component, OnInit } from '@angular/core';
import { DashboardService } from '../../statistics/dashboard.service';
import { TopSales, PaginationTopSales, TopSalesYear, PaginationTopSalesYear } from '.././dashboard';
import { Data as Order , Pagination as OrderPagination } from '../../orders/orders';
import { Data as Behavior , Pagination as BehaviorPagination } from '../../behaviors/behaviors';
import { BehaviorsService } from '../../behaviors/behaviors.service'
import { Router } from '@angular/router';
import { Globals } from "../../../../../system/components/main/globals";
@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.css']
})
export class StatisticsComponent implements OnInit {
  page:number;
  top_sales_year: TopSalesYear[];
  recent_orders: Order[];
  top_selled_products: TopSales[];
  searches: Behavior[];
  query:string;
  pages_top_selled: number;
  pages_top_sales: number;
  pages_recent_orders: number;
  pages_searches: number;
  constructor(
    private router: Router, 
    private categories:DashboardService,
    private behaviors:BehaviorsService,
    private global:Globals
    ) { }
  ngOnInit() {
    //inject dashboard
    this.global.initializeActions();
    this.loadTopsales();  
    this.loadRecentOrders();
    this.loadTopselledProducts();
    this.loadSearches();
  }
 
  loadSearches()
  {
     // load top sales
    this.pages_searches = 0;
    this.behaviors.List(1).subscribe((data:BehaviorPagination) => {

        data.items.forEach(obj => {
            obj.checked = false;
            obj.selected = false;
         })
       this.searches = data.items ;
    });
  }
  loadTopsales()
  {
     // load top sales
    this.pages_top_sales = 0;
    this.categories.TopSalesYear().subscribe((data:TopSalesYear[]) => {

        data.forEach(obj => {
            obj.checked = false;
            obj.selected = false;
         })
       this.top_sales_year = data ;
    });
  }

  loadTopselledProducts()
  {
     // load top sales
    this.pages_top_selled = 0;
    this.categories.TopSelledProducts().subscribe((data:PaginationTopSales) => {

      data.items.forEach(obj => {
            obj.checked = false;
            obj.selected = false;
         })
       this.top_selled_products = data.items ;
       this.pages_top_sales = data.pages;
    });
  }

  loadRecentOrders()
  {
    // load recent orders
    this.pages_recent_orders = 0;
    this.categories.RecentOrders().subscribe((data:OrderPagination) => {

      data.items.forEach(obj => {
            obj.checked = false;
            obj.selected = false;
         })
       this.recent_orders = data.items ;
       this.pages_recent_orders = data.pages;
    });
  }
}

import { Component, OnInit,AfterViewInit,Input } from '@angular/core';
import { Data, ProductTypes} from '.././post'
import { Observable } from 'rxjs';
import { take, switchMap, combineAll,map } from 'rxjs/operators';
import { PostService } from '../../post/post.service'
import { Router,ActivatedRoute } from '@angular/router';
import { CustomMethodsService } from '../../../../../libraries/utils/custommethods.service';
import { Globals } from "../../../../../system/components/main/globals";
import { AdviableVar } from '../../../../../libraries/utils/customvars';
import { ElementRef,  ViewChild } from '@angular/core';
import {Data as Category}  from '../../categories/category'; 
import {Data as Tags}  from '../../../../../system/components/tags/tags'; 
import { BaseEditComponent } from "../../../../../libraries/utils/components/baseedit.component";

const SimpleMDE: any = require('simplemde');
declare var jQuery:any;
declare var $:any;
@Component({
  selector: 'app-article_post_edit',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class ArticlesPostEditComponent  extends BaseEditComponent implements OnInit{
  model:Data;
  mde:any;
  cats:Category[];
  tags:Tags[];
  related:number[];
  marketplace:number;
  product_types: ProductTypes[];
  exclude:number;
  @ViewChild('simplemde') textarea: ElementRef

  constructor(  
    private custom_methodsx:CustomMethodsService,
    private routerx: Router,
    private routex:ActivatedRoute, 
    private categoriesx:PostService,
    private globalx:Globals) {
      super(custom_methodsx,routerx,routex,categoriesx,globalx);
      this.list_url = "/articles/posts"
    }



  ngOnInit() {
      
      
      this.globalx.initializeActions();
      this.initialize()
      this.initializeEditor();
      //asynchronous

       
       this.routex.params.subscribe(params =>{

        if (typeof params['id'] !== 'undefined') 
        {       
          //asynchronous
            this.exclude = params['id']
            this.categoriesx.Get(params['id']).subscribe((data:Data) => {
            this.model = data;
            this.marketplace = data.marketplace_id;
            this.cats = data.categories_lists;
            this.tags = data.tags_lists;
            this.model.releated_posts = data.related_postsx;
            this.related = data.related_postsx;
            this.mde.codemirror.setValue(this.model.content);
            this.globalx.actions.is_new = false;
          });
        }
        
      });
    
        
  }
  initialize()
  {
      this.exclude = 0;
      this.cats = [];
      this.tags = [];
      this.related = [];
      this.marketplace = 0;
      this.product_types = [
        {
          id:"physical",
          name:"PHYSICAL_LABEL"
        },
        {
          id:"digital",
          name:"DIGITAL_LABEL"
        },
        {
          id:"downloable",
          name:"DOWNLOABLE_LABEL"
        },
      ];

      this.model =  {
      marketplace:0,
      marketplace_id:0,
      title:"",
      meta_title:"",
      meta_description:'',
      slug:"",
      sku:"",
      thumbnail:'',
      thumbnail_text:'',
      featured_image:'',
      featured_image_text:'',
      content:"",
      excerpt:'',
      publish_date:"",
       //featured_start_date:'',
       //featured_end_date:'',      
      id:null,
      publish:true,
      checked:false,
      is_featured:false,
      is_on_feed:false,
      tag_lists:null,
      tags_lists:null,
      categories_lists:null,
      related_postsx: null,
      releated_posts: null,
      selected:false,
      product_type:"",
      price:0,
      qty:0,
      currency:"USD"
   
    };
  }
 
  setContent(value:string)
  {
    this.model.content = value;
  }
  setPublishDate(value:string)
  {
    this.model.publish_date = value;
  }
  initializeEditor()
  {
 
    var mde = new SimpleMDE({
      element: this.textarea.nativeElement,
      forceSync: true,
      spellChecker: false,
      status: true
    });
    
   //pass the current instance
    var setValue = this;
    var ChangeEditor = function()
    {

      var value = mde.codemirror.getValue();
    
      setValue.setContent(value) ;  
      
    }
    mde.codemirror.on('change',ChangeEditor);
 
    this.mde = mde;
     function datetimepicker10 (e) {
      setValue.setPublishDate($('#datetimepicker10 input').val())   ;
     }
    $('#datetimepicker10').datetimepicker({ viewMode: 'years',format: 'YYYY-MM-DD H:mm:ss'}).on("dp.change",datetimepicker10 );
     
  }
  


  save()
  {  
      var that = this;
      
      this.model.categories_lists = this.cats;
      this.model.tag_lists = this.tags;
      this.model.releated_posts = this.related;
      this.model.marketplace = this.marketplace;
      var disable_updated_box = function() {
        that.globalx.actions.updated = false;
      }
      if (this.globalx.actions.is_new == false)
      {
       
        this.categoriesx.Update(this.model).subscribe((data:any) => {

          this.globalx.actions.updated = true;
          setTimeout(disable_updated_box, 3000);
        });
      }else
      {
        this.categoriesx.New(this.model).subscribe((data:Data) => {
          this.model = data;   
          this.globalx.actions.is_new = false;   
          this.globalx.actions.updated = true;
          setTimeout(disable_updated_box, 3000);
        });
      }
 
     
    
  }

}

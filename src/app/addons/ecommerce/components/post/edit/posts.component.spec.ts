import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArticlesPostEditComponent } from './post.component';

describe('ArticlesPostEditComponent', () => {
  let component: ArticlesPostEditComponent;
  let fixture: ComponentFixture<ArticlesPostEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArticlesPostEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArticlesPostEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { GalleryService } from '../../post/gallery.service'
import { Data,Pagination,GalleryTypes } from '.././gallery'
import { Router } from '@angular/router';
import { Globals } from "../../../../../system/components/main/globals";
import { AfterViewInit, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
declare var jQuery:any;
declare var $:any;
@Component({

  selector: 'app-article_posts_selectable_media',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css']
})
export class ArticlesPostSelectableMediaComponent implements AfterViewInit {

  
  page:number = 1;
  model2:Data;
  items: Data[];
  classification_types:GalleryTypes[];
  anotation_positon_types:GalleryTypes[];
  query:string = "";
  selected:boolean = false;
  selectedable:boolean = false;
  pages: number = 0;
  progresss: number;
  @Input() model: number;

  constructor(
    private router: Router, 
    private post:GalleryService,
    private global:Globals,
    private elementRef: ElementRef) { this.model = 0; this.initialize();}
  ngAfterViewInit() {
    //inject dashboard
    this.global.initializeActions();
   
    this.loadnewThemes();
  }
    initialize()
  {

      this.classification_types = [
        {
          id:"video",
          name:"video"
        },
        {
          id:"image",
          name:"image"
        },
         {
          id:"thumbnail",
          name:"thumbnail"
        }
      ];
    this.anotation_positon_types = [
        {
          id:"left",
          name:"left"
        },
        {
          id:"right",
          name:"right"
        }
      ];
      this.model2 =  {
        title:"",
        link:"",
        anotation:"",
        product_id:0,
        clasification:"",
        featured:false,
        anotation_positon:"",
        id:null,
        selected:false,
        checked:false,
        editable:false
    };
  }

  UPDATE(item:Data){
    var that = this;
    item.editable = false;
    item.product_id = this.model;
    var disable_updated_box = function() {
      that.global.actions.updated = false;
    }
    this.post.Update(item).subscribe((data:any) => {

      this.global.actions.updated = true;
      setTimeout(disable_updated_box, 3000);
    });
  }

  save()
  {  
    var that = this;
    that.model2.product_id = this.model;
    var disable_updated_box = function() {
      that.global.actions.updated = false;
    }
     
    this.post.New(this.model2).subscribe((data:Data) => {
      this.model2 = data;   
      this.global.actions.updated = true;
       this.loadnewThemes();
      setTimeout(disable_updated_box, 3000);
    });
  }

  loadnewThemes()
  {
    if(typeof this.model !== "undefined")
    {
      this.post.List(this.page,this.model).subscribe((data:Pagination) => {
        data.items.forEach(obj => {
            obj.checked = false;
            obj.selected = false;
            obj.editable = false
         })
        this.items = data.items;
        this.pages = data.pages;
      });
    }
    
  }

  loadmore()
  {
    this.page ++;
    if( this.page >= this.pages)
    {
      this.page = this.pages;
    }
    if(this.query.length >0 )
    {
        this.post.Search(
          this.page,
          this.query,
          this.model
        ).subscribe((data:Pagination) => {
          data.items.forEach(obj => {
            obj.checked = false;
            obj.selected = false;
            obj.editable = false
         })
         this.items.concat(data.items) ;
        this.pages = data.pages;
      });
    }else
    {
      this.post.List(this.page,this.model).subscribe((data:Pagination) => {
          data.items.forEach(obj => {
            obj.checked = false;
            obj.selected = false;
            obj.editable = false
         })
        this.pages = data.pages;
       
        this.items.concat(data.items) ;
      });
    }
   
  }

  SELECT_ALL(){
    if(this.selected == false)
    {
      
     this.items.forEach(obj => {
            obj.checked = true;
         })
     
   }else
   {

     this.items.forEach(obj => {
            obj.checked = false;
         })
   }
  }
  DELETED_ALL(){
    
      var that = this;
      var disable_updated_box = function() {
        that.global.actions.deleted = false;
      }

      this.items.filter(obj => obj.checked == true).forEach(item => {
        this.post.Delete(item.id).subscribe((data:any) => {
          this.global.actions.deleted = true;
          this.items = this.items.filter(objx => objx !== item);
           setTimeout(disable_updated_box, 3000);
        });
      })
  }
  SET_FEATURED(item:Data){
    if(item.editable == true)
    {
        if(item.featured == false)
        {
          item.featured = true;
        }else{
          item.featured = false;
        }
     }
   
  }
  DELETE(item:Data){

      var that = this;
      var disable_updated_box = function() {
        that.global.actions.deleted = false;
      }
      this.global.actions.deleted = true;
          
      this.post.Delete(item.id).subscribe((data:any) => {
        setTimeout(disable_updated_box, 3000);
      });
      this.items = this.items.filter(obj => obj !== item);
  }
  EDIT(item:Data)
  {
    if(item.editable == false)
    {
      item.editable = true;
    }else{
      item.editable = false;
    }
  }
  find()
  {
 
  
    this.page = 1;

    if(this.query.length > 0 )
    {
        this.post.Search(
          this.page,
          this.query,
          this.model
        ).subscribe((data:Pagination) => {
           data.items.forEach(obj => {
            obj.checked = false;
            obj.selected = false;
            obj.editable = false
         })
         this.items = data.items ;
          this.pages = data.pages;
      });
    }else
    {
      this.post.List(this.page,this.model).subscribe((data:Pagination) => {
        data.items.forEach(obj => {
            obj.checked = false;
            obj.selected = false;
            obj.editable = false
         })
        this.items = data.items ;
         this.pages = data.pages;
      });
    }
  }


 



}

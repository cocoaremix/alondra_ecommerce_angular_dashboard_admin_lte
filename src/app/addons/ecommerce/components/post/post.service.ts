import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, } from '@angular/common/http';
import { Headers } from '@angular/http';
import { RequestOptions, Request, RequestMethod} from '@angular/http';
import { environment } from '../../../../../environments/environment';
import { Globals } from "../../../../system/components/main/globals";
import { CookieService } from 'ngx-cookie-service';
import { HttpJsonHeaderOptionsService } from '../../../../libraries/utils/httpoptions.service';
import { BaseService } from "../../../../libraries/utils/components/base.service";

@Injectable({
  providedIn: 'root'
})
export class PostService  extends BaseService {

   constructor(
    private httpx: HttpClient, 
    private cookiex:CookieService,
    private globalx:Globals, 
    private headerx: HttpJsonHeaderOptionsService) { 
      super(httpx,cookiex, globalx, headerx);
      this.list_url="articles/";
      this.search_url="articles/search/";
      this.new_url="article/";
      this.get_url="article/details/";
      this.find_url="article/find/";
      this.find_slug_url="article/find/slug/";
      this.update_url="article/";
      this.delete_url="article/";
  }
 
  ListType(page,product_type) {
     
    const postedData = {page:page,product_type:product_type}
    return this.httpx
      .post( 
        environment.url + "articles/",
        postedData,
        this.headerx.get_auth(this.globalx.token)
      );
  }

  SearchType(page, query,product_type) {
    const postedData = {
      page:page,
      query:query,
      product_type:product_type
    }
    return this.httpx
      .post( 
        environment.url + "articles/search/",
        postedData,
        this.headerx.get_auth(this.globalx.token)
      );
  }

  Sear
}

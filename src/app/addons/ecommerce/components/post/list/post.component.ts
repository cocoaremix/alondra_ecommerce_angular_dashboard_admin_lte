import { Component, OnInit } from '@angular/core';
import { PostService } from '../../post/post.service'
import { Data,Pagination } from '.././post'
import { Router } from '@angular/router';
import { Globals } from "../../../../../system/components/main/globals";
import { BaseListItemsComponent } from "../../../../../libraries/utils/components/baselist.component";

@Component({
  selector: 'app-article_post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class ArticlesPostComponent extends BaseListItemsComponent  {
    page:number;
    items: Data[];
    query:string;
    selected:boolean;
    pages: number;
    constructor(
      private  routerx: Router, 
      private globalx:Globals,
      private categoriesx:PostService
    ) { 
      super(routerx,globalx,categoriesx);
      this.new_url='articles/posts/new/';
      this.edit_url ='articles/posts/edit/';
   }
 
}

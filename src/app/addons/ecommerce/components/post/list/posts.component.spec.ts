import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArticlesPostComponent } from './post.component';

describe('ArticlesPostComponent', () => {
  let component: ArticlesPostComponent;
  let fixture: ComponentFixture<ArticlesPostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArticlesPostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArticlesPostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

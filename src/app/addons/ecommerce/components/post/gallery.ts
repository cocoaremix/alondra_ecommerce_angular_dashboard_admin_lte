
import {Data as Category}  from '../categories/category'; 
import {Data as Tags}  from '../../../../system/components/tags/tags'; 

export interface GalleryTypes {
	id: string;
	name:string;
}

export interface Data {
	id: number;
	product_id:number;
	title:string;
	link:string;
	clasification:string;
	anotation_positon:string;
	featured:boolean;
	anotation:string;
	selected:boolean;
	editable:boolean;
	checked:boolean;
}


export interface Pagination {
	pages: number;
	items: Data[];
	next_page: string;
	prev_page: string;
}



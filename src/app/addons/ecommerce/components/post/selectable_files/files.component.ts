import { Component, OnInit } from '@angular/core';
import { MediaFilesService } from '../../post/media_files.service'
import { Data,Pagination,Succcess } from '.././media_files'
import { Router } from '@angular/router';
import { Globals } from "../../../../../system/components/main/globals";
import { AfterViewInit, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { BaseItemSelectableNumberComponent } from "../../../../../libraries/utils/components/baseselectable.component";

declare var jQuery:any;
declare var $:any;
@Component({

  selector: 'app-article_posts_selectable_files',
  templateUrl: './files.component.html',
  styleUrls: ['./files.component.css']
})
export class ArticlesPostSelectableFilesComponent extends BaseItemSelectableNumberComponent  implements AfterViewInit {

  
  page:number = 1;
  items: Data[];
  query:string = "";
  selected:boolean = false;
  selectedable:boolean = false;
  pages: number = 0;
  progresss: number;
  @Input() model: number;
  
  constructor(
    private routerx: Router,
    private globalx:Globals,
    private postx:MediaFilesService,
    private elementRefx: ElementRef) { 
      super(routerx,globalx,postx,elementRefx);
  }
  ngAfterViewInit() {
    //inject dashboard
    this.globalx.initializeActions();
    this.initializeEditor();
    this.loadnewThemes();
  }

  loadnewThemes()
  {
    if(typeof this.model !== "undefined")
    {
      this.postx.List(this.page,this.model).subscribe((data:Pagination) => {
        data.items.forEach(obj => {
            obj.checked = false;
            obj.selected = false;
            obj.selectedable = false;
         })
        this.items = data.items;
        this.pages = data.pages;
      });
    }
    
  }

  loadmore()
  {
    this.page ++;
    if( this.page >= this.pages)
    {
      this.page = this.pages;
    }
    if(this.query.length >0 )
    {
        this.postx.Search(
          this.page,
          this.query,
          this.model
        ).subscribe((data:Pagination) => {
          data.items.forEach(obj => {
            obj.checked = false;
            obj.selected = false;
            obj.selectedable = false;
         })
         this.items.concat(data.items) ;
        this.pages = data.pages;
      });
    }else
    {
      this.postx.List(this.page,this.model).subscribe((data:Pagination) => {
         data.items.forEach(obj => {
            obj.checked = false;
            obj.selected = false;
            obj.selectedable = false;
         })
        this.pages = data.pages;
       
        this.items.concat(data.items) ;
      });
    }
   
  }




  find()
  {
 
  
    this.page = 1;

    if(this.query.length > 0 )
    {
        this.postx.Search(
          this.page,
          this.query,
          this.model
        ).subscribe((data:Pagination) => {

         this.items = data.items ;
          this.pages = data.pages;
      });
    }else
    {
      this.postx.List(this.page,this.model).subscribe((data:Pagination) => {

        this.items = data.items ;
         this.pages = data.pages;
      });
    }
  }


  Upload(files:FileList[]) {
      var fd = new FormData();
      fd.append("product_id", this.model.toString() );
      jQuery.each(files,function(i,val){
        fd.append("file",val)
      })
      this.postx.Upload(fd).subscribe( (event: Succcess) => {
        if(  event.success == true)
        {
          this.loadnewThemes();
        }
      });
      
  };
  initializeEditor()
  { 
    /*
      pending create ajax with angular progressbar ajax put
    */       

    this.progresss = 0;
    var that = this;
   
    var open_uploader = function (e)
    {  
      e.preventDefault();
      e.stopPropagation();
      jQuery('#id_files').focus().trigger('click');
    }

    jQuery("#holder").on('click',open_uploader);
    var clicks = function(e)
    {
      e.stopPropagation();
    }
    jQuery('#id_files').on('click',clicks);
    
    jQuery('#id_files').bind('change', function (e) {
      e.stopPropagation();
      var files:FileList[] = e.target.files;
      that.Upload(files);
    });

    jQuery("#holder").on('drop', function(e) {
      e.preventDefault();
      e.stopPropagation();
      
      if (e.dataTransfer){
          if (e.dataTransfer.files.length > 0) {
             var files:FileList[] = e.dataTransfer.files;
              that.Upload(files);
          }
      }
      return false;
    });
    jQuery("#holder").on('dragover', function(e) {
        e.preventDefault();
        e.stopPropagation();
    });
    jQuery("#holder").on('dragenter', function(e) {
        e.preventDefault();
        e.stopPropagation();
    });

  }



}

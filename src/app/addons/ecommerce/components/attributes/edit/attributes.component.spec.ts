import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AttributesEditComponent } from './attributes.component';

describe('AttributesEditComponent', () => {
  let component: AttributesEditComponent;
  let fixture: ComponentFixture<AttributesEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AttributesEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttributesEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

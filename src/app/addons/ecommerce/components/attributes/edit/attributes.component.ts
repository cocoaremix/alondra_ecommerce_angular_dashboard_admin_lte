import { Component, OnInit } from '@angular/core';
import { Data,ArcheTypes } from '.././attributes'
import { AttibutesService } from '.././attributes.service'
import { Router,ActivatedRoute } from '@angular/router';
import { CustomMethodsService } from '../../../../../libraries/utils/custommethods.service';
import { AdviableVar } from '../../../../../libraries/utils/customvars';
import { Globals } from "../../../../../system/components/main/globals";

@Component({
  selector: 'app-attributes_edit',
  templateUrl: './attributes.component.html',
  styleUrls: ['./attributes.component.css']
})
export class AttributesEditComponent implements OnInit {
  model:Data;
  archetypes: ArcheTypes[];
  constructor(  
    private custom_methods:CustomMethodsService,
    private router: Router,
    private route:ActivatedRoute, 
    private categories:AttibutesService,
    private global:Globals) { }


  ngOnInit() {
      this.global.initializeActions();
      this.initialize();
      this.route.params.subscribe(params =>{

        if (typeof params['id'] !== 'undefined') 
        {       
          
          this.categories.Get(params['id']).subscribe((data:Data) => {
          this.model = data;
          this.global.actions.is_new = false;
        });
        }
        
      });
    
  }
  initialize()
  {


    this.archetypes = [
      {
        id:"choices",
        name:"choices"
      },
      {
        id:"text",
        name:"text"
      },
      {
        id:"selectable",
        name:"selectable"
      },
      {
        id:"date",
        name:"date"
      },
    ]

    this.model =  {
      archetype:"",
      name: "",
      child:[],
      priceable:false,
      id:null,
      checked:false,
      selected:false,
    };
  }
  save()
  {  
      var that = this;
      var disable_updated_box = function() {
        that.global.actions.updated = false;
      }
      if (this.global.actions.is_new == false)
      {
        this.categories.Update(this.model).subscribe((data:any) => {

          this.global.actions.updated = true;
          setTimeout(disable_updated_box, 3000);
        });
      }else
      {
        this.categories.New(this.model).subscribe((data:Data) => {
          this.model = data;   
          this.global.actions.is_new = false;   
          this.global.actions.updated = true;
          setTimeout(disable_updated_box, 3000);
        });
      }
    
     
    
  }
  
 
  back()
  {
     this.router.navigate(['/attributes' ]);
  }
  DELETE()
  {
   
    this.categories.Delete(this.model.id).subscribe((data:any) => {
      this.router.navigate(['/attributes' ]);

    });
    
  }

}

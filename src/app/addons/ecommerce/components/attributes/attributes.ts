
export interface ArcheTypes {
	id: string;
	name:string;
}

export interface ProductAttributes {
	id: number;
	thumbnail:string;
	thumbnail_text:string;
	featured_image:string;
	featured_image_text:string;
	product_id:number;
	attributes_id:number;
	value:string;
	price:number;
	checked: boolean;
	selected: boolean;
}


export interface Data {
	id: number;
	archetype:string;
	name:string;
	child:ProductAttributes[];
	priceable:boolean;
	checked: boolean;
	selected: boolean;
}



export interface Pagination {
	pages: number;
	items: Data[];
	next_page: string;
	prev_page: string;
}
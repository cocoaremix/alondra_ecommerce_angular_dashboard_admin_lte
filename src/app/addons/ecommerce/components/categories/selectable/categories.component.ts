import { Component, OnInit } from '@angular/core';
import { ArticlesCategoryService } from '../../categories/category.service'
import { Data,Pagination } from '.././category'
import { Router } from '@angular/router';
import { Globals } from "../../../../../system/components/main/globals";
import { AfterViewInit, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { BaseListItemSelectableComponent } from "../../../../../libraries/utils/components/baseselectable.component";

@Component({
  selector: 'app-articles_categories_selectable',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class ArticlesCategoriesSelectableComponent extends BaseListItemSelectableComponent {

  items: Data[];
  @Input() model: Data[];
  @Output() modelChange = new EventEmitter<Data[]>();
  constructor(
      private routerx: Router, 
      private globalx:Globals,
      private categoriesx:ArticlesCategoryService,
      private elementRefx: ElementRef
    ) {

      super(routerx, globalx,categoriesx, elementRefx);
      this.edit_url = '/articles/categories/edit/';
      this.new_url ='/articles/categories/new';
      
   }

 
}

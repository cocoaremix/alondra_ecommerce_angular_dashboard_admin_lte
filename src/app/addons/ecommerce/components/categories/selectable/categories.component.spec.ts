import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArticlesCategoriesSelectableComponent } from './categories.component';

describe('ArticlesCategoriesSelectableComponent', () => {
  let component: ArticlesCategoriesSelectableComponent;
  let fixture: ComponentFixture<ArticlesCategoriesSelectableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArticlesCategoriesSelectableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArticlesCategoriesSelectableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

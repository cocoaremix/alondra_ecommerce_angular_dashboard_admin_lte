import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArticlesCategoriesEditComponent } from './categories.component';

describe('ArticlesCategoriesEditComponent', () => {
  let component: ArticlesCategoriesEditComponent;
  let fixture: ComponentFixture<ArticlesCategoriesEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArticlesCategoriesEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArticlesCategoriesEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

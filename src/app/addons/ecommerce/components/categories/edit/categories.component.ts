import { Component, OnInit } from '@angular/core';
import { Data } from '.././category'
import { ArticlesCategoryService } from '.././category.service'
import { Router,ActivatedRoute } from '@angular/router';
import { CustomMethodsService } from '../../../../../libraries/utils/custommethods.service';
import { AdviableVar } from '../../../../../libraries/utils/customvars';
import { Globals } from "../../../../../system/components/main/globals";
import { BaseEditComponent } from "../../../../../libraries/utils/components/baseedit.component";

@Component({
  selector: 'app-article_categories_edit',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class ArticlesCategoriesEditComponent extends BaseEditComponent implements OnInit {
  model:Data;

  constructor(  
    private custom_methodsx:CustomMethodsService,
    private routerx: Router,
    private routex:ActivatedRoute, 
    private categoriesx:ArticlesCategoryService,
    private globalx:Globals) {
      super(custom_methodsx,routerx,routex,categoriesx,globalx);
      this.list_url = "/articles/categories/"
    }

    initialize()
    {
      this.model =  {
        name:"",
        meta_title:"",
        meta_description:'',
        slug:"",
        id:null,
        publish:true,
        checked:false,
        selected:false,
      };
    }
     FindSlug()
    {
      this.categoriesx.FindSlug(this.model.slug).subscribe((data:AdviableVar) => {

        this.globalx.actions.exists_slug = data;
      });
    }
    FindTitle()
    {
      this.categoriesx.Find(this.model.name).subscribe((data:AdviableVar) => {

         this.globalx.actions.exists_title = data;
         this.globalx.actions.exists_slug = data;

      });
    }
    TitleChanged()
    {
      this.model.slug = this.custom_methodsx.string_to_slug(this.model.name);
      this.model.meta_title = this.model.name;
    } 

}

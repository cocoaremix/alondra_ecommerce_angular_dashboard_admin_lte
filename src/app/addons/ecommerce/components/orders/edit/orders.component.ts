import { Component, OnInit } from '@angular/core';
import { Data,Statuses } from '.././orders';
import { OrdersService } from '.././orders.service';
import { Data as User}  from '../../../../../system/components/users/users';
import { UsersService } from '../../../../../system/components/users/users.service';
import { Data as OrdersItems,Addresess,CdKeysOrders,EmailsDigitalDelivery } from '.././orders_items';
import { CustomMethodsService } from '../../../../../libraries/utils/custommethods.service';
import { OrdersItemsService } from '.././orders_items.service';
import { CdkeysService } from '../../cd_keys/cd_keys.service';
import { Router,ActivatedRoute } from '@angular/router';
import { AdviableVar } from '../../../../../libraries/utils/customvars';
import { Globals } from "../../../../../system/components/main/globals";

@Component({
  selector: 'app-orders_edit',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersEditComponent implements OnInit {
  model:Data;
  items:OrdersItems[];
  statuses:Statuses[];
  initialized_address:any;
  media_url:string;

  constructor(  
    private custom_methods:CustomMethodsService,
    private router: Router,
    private route:ActivatedRoute, 
    private categories:OrdersService,
    private orderitems:OrdersItemsService,
    private cd_keys:CdkeysService,
    private userservice:UsersService,
    private global:Globals) { }


  ngOnInit() {
      this.global.initializeActions();
      this.initialize();
      this.route.params.subscribe(params =>{
        if (typeof params['id'] !== 'undefined') 
        { 
          this.global.actions.is_new = false;
          this.categories.Get(params['id']).subscribe((data:Data) => {
            this.model = data;
            this.model.selected = false;
            this.model.checked = false;
            this.orderitems.List(data.id).subscribe((data2:OrdersItems[]) => {
              this.items = data2
               this.items.forEach(obj => {
                  obj.checked = false;
                  obj.selected = false;
                  this.cd_keys.OrderItemCdKeys(obj.id).subscribe((data3:CdKeysOrders[]) => {
                    obj.keys = data3;
                  })
               })
            });
          });
        }
      });
    
  }
  initialize()
  {
    this.media_url = this.global.media_url;
    this.initialized_address = {
        id: 0,
        city: "",
        country: "",
        zip_code: "",
        first_name: "",
        last_name: "",
        address_line_1: "",
        address_line_2: "",
        address_type: "",
        created: null,
        modified: null,
        checked: false,
        selected: false
      };

    this.statuses = [
      {
        id:"approved",
        name:"approved"
      },
      {
        id:"pending",
        name:"pending"
      },
      {
        id:"refunded",
        name:"refunded"
      },
      {
        id:"processed",
        name:"processed"
      },
    ];
    this.model =  {
      status:"",
      order_id:0,
      autorx:{
        id:null,
        role:"",
        first_name:"",
        last_name:"",
        is_active:true,
        is_superuser:false,
        username:"",
        email:"",
        nick:"",
        checked:false,
        password:"",
        parnetship_requested:false,
        parnetship:true,
      },
      autor_id:0,
      currency:"",
      payment_methodx:[],
      id:null,
      created:"",
      modified:"",
      checked:false,
      selected:false,
    };
  }
  save()
  {  
    var that = this;
    var disable_updated_box = function() {
      that.global.actions.updated = false;
    }
    if (this.global.actions.is_new == false)
    {
      this.categories.Update(this.model).subscribe((data:any) => {
        this.global.actions.updated = true;
        setTimeout(disable_updated_box, 3000);
      });
    }else
    {
      this.categories.New(this.model).subscribe((data:Data) => {
        this.model = data;   
        this.global.actions.is_new = false;   
        this.global.actions.updated = true;
        setTimeout(disable_updated_box, 3000);
      });
    }
  }
  back()
  {
    this.router.navigate(['/orders' ]);
  }
  DELETE()
  {
    this.categories.Delete(this.model.id).subscribe((data:any) => {
      this.router.navigate(['/orders' ]);
    });
  }

  EDIT_STATUS(item:Data)
  {
     if(item.selected == false)
    {
      item.selected =true;
    }else{
      item.selected = false;
      var that = this;
      var disable_updated_box = function() {
        that.global.actions.updated = false;
      }
      this.categories.Update(this.model).subscribe((data:any) => {
        this.global.actions.updated = true;
        setTimeout(disable_updated_box, 3000);
      });
    }
  }

  EDIT_EMAIL_DELIVERY_SENDED(item:EmailsDigitalDelivery)
  {
    if(item.email_sent == false)
    {
      item.email_sent = true;
      this.orderitems.UpdateShippingEmail(item).subscribe((data:any) => {});
    }else{
      item.email_sent = false;
    }

  }

  ADD_TRACKING(item:OrdersItems)
  {
    if(item.selected == false)
    {
      item.selected =true;
    }else{
      item.selected = false;
        this.orderitems.UpdateTracking(item).subscribe((data:any) => { });
    }
  }

}

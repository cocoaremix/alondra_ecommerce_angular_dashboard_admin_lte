import {Data as User}  from '../../../../system/components/users/users'

export interface PaymentMethodFields {
	id: number;
	key:string;
	value:string;
}

export interface Statuses {
	id: string;
	name:string;
}



export interface PaymentMethod {
	id: number;
	payment_method:string;
	payment_info:PaymentMethodFields[];
	amount:number;
	currency:string;
	order_ids:number;
	created:string,
	modified:string,
	checked: boolean;
	selected: boolean;
}

export interface Data {
	id: number;
	status:string;
	order_id:number;
	autorx:User;
	autor_id:number;
	currency:string;
	payment_methodx:PaymentMethod[];
	created:string,
	modified:string,
	checked: boolean;
	selected: boolean;
}


export interface Pagination {
	pages: number;
	items: Data[];
	next_page: string;
	prev_page: string;
}






export interface UserMostViewedProducts {
	id: number;
	title:string;
	thumbnail:string;
	featured_image:string;
	email:string;
	first_name:string;
	last_name:string;
	username:string;
	logo:string;
	views:number;
	checked: boolean;
	selected: boolean;
}

export interface UserMostViewedProductsPagination {
	pages: number;
	items: UserMostViewedProducts[];
	next_page: string;
	prev_page: string;
}

export interface Data {
	id: number;
	views: number;
	title:string;
	thumbnail:string;
	featured_image:string;
	checked: boolean;
	selected: boolean;
}

export interface Pagination {
	pages: number;
	items: Data[];
	next_page: string;
	prev_page: string;
}



export interface Data {
	id: number;
	views: number;
	term:string;
	checked: boolean;
	selected: boolean;
}

export interface Pagination {
	pages: number;
	items: Data[];
	next_page: string;
	prev_page: string;
}
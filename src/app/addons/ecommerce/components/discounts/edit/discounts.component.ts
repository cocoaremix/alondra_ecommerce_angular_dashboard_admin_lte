import { Component, OnInit } from '@angular/core';
import { Data } from '.././discounts'
import { DiscountsService } from '.././discounts.service'
import { Router,ActivatedRoute } from '@angular/router';
import { CustomMethodsService } from '../../../../../libraries/utils/custommethods.service';
import { AdviableVar } from '../../../../../libraries/utils/customvars';
import { Globals } from "../../../../../system/components/main/globals";

@Component({
  selector: 'app-discounts_edit',
  templateUrl: './discounts.component.html',
  styleUrls: ['./discounts.component.css']
})
export class DiscountsEditComponent implements OnInit {
  model:Data;
  constructor(  
    private custom_methods:CustomMethodsService,
    private router: Router,
    private route:ActivatedRoute, 
    private categories:DiscountsService,
    private global:Globals) { }


  ngOnInit() {
      this.global.initializeActions();
      this.initialize();
      this.route.params.subscribe(params =>{

        if (typeof params['id'] !== 'undefined') 
        {       
          
          this.categories.Get(params['id']).subscribe((data:Data) => {
          this.model = data;
          this.global.actions.is_new = false;
        });
        }
        
      });
    
  }
  initialize()
  {
    this.model =  {
      code:"",
      amount:0,
      id:null,
      checked:false,
      selected:false,
    };
  }
  save()
  {  
      var that = this;
      var disable_updated_box = function() {
        that.global.actions.updated = false;
      }
      if (this.global.actions.is_new == false)
      {
        this.categories.Update(this.model).subscribe((data:any) => {

          this.global.actions.updated = true;
          setTimeout(disable_updated_box, 3000);
        });
      }else
      {
        this.categories.New(this.model).subscribe((data:Data) => {
          this.model = data;   
          this.global.actions.is_new = false;   
          this.global.actions.updated = true;
          setTimeout(disable_updated_box, 3000);
        });
      }
    
     
    
  }
  back()
  {
     this.router.navigate(['/discounts' ]);
  }
  DELETE()
  {
   
    this.categories.Delete(this.model.id).subscribe((data:any) => {
      this.router.navigate(['/discounts' ]);

    });
    
  }

}

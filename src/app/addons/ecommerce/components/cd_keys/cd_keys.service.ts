import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, } from '@angular/common/http';
import { Headers } from '@angular/http';
import {RequestOptions, Request, RequestMethod} from '@angular/http';
import { environment } from '../../../../../environments/environment';
import { Globals } from "../../../../system/components/main/globals";
import { CookieService } from 'ngx-cookie-service';
import { HttpJsonHeaderOptionsService } from '../../../../libraries/utils/httpoptions.service';
import { BaseService } from "../../../../libraries/utils/components/base.service";

@Injectable({
  providedIn: 'root'
})
export class CdkeysService  extends BaseService {

   constructor(
    private httpx: HttpClient, 
    private cookiex:CookieService,
    private globalx:Globals, 
    private headerx: HttpJsonHeaderOptionsService) { 
      super(httpx,cookiex, globalx, headerx);
      this.list_url="cd_keys/";
      this.search_url="cd_keys/search/";
      this.new_url="cd_key/";
      this.get_url="cd_key/details/";
      this.find_url="cd_key/find/";
      this.find_slug_url="cd_key/find/slug/";
      this.update_url="cd_key/";
      this.delete_url="cd_key/";
  }
 
  OrderItemCdKeys(order_item_id) {
     
    const postedData = {order_item_id:order_item_id}
    return this.httpx
      .post( 
        environment.url + "cd_keys/list/order/item/",
        postedData,
        this.headerx.get_auth(this.globalx.token)
      );
  }
}

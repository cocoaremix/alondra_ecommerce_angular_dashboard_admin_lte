import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CdkeysEditComponent } from './cd_keys.component';

describe('CdkeysEditComponent', () => {
  let component: CdkeysEditComponent;
  let fixture: ComponentFixture<CdkeysEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CdkeysEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CdkeysEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

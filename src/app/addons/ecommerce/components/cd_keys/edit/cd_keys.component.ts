import { Component, OnInit } from '@angular/core';
import { Data } from '.././cd_keys'
import { CdkeysService } from '.././cd_keys.service'
import { CustomMethodsService } from '../../../../../libraries/utils/custommethods.service';
import { Router,ActivatedRoute } from '@angular/router';
import { AdviableVar } from '../../../../../libraries/utils/customvars';
import { Globals } from "../../../../../system/components/main/globals";
import { BaseEditComponent } from "../../../../../libraries/utils/components/baseedit.component";

@Component({
  selector: 'app-cd_keys_edit',
  templateUrl: './cd_keys.component.html',
  styleUrls: ['./cd_keys.component.css']
})
export class CdkeysEditComponent extends BaseEditComponent implements OnInit {
  model:Data;
  related:number;
  constructor(  
    private custom_methodsx:CustomMethodsService,
    private routerx: Router,
    private routex:ActivatedRoute, 
    private categoriesx:CdkeysService,
    private globalx:Globals) {
      super(custom_methodsx,routerx,routex,categoriesx,globalx);
      this.list_url = "/cd_keys/"
  }
  ngOnInit() {
      this.globalx.initializeActions();
      this.initialize();
      this.routex.params.subscribe(params =>{

        if (typeof params['id'] !== 'undefined') 
        {       
          
          this.categoriesx.Get(params['id']).subscribe((data:Data) => {
            this.model = data;
            this.related = data.product_idx;
            this.globalx.actions.is_new = false;
          });
        }
        
      });
    
  }

  initialize()
  {
    this.related = 0;
    this.model =  {
      productx:"",
      product_idx:0,
      product_id:0,
      cd_key:"",
      status:"pending",
      id:null,
      checked:false,
      selected:false,
    };
  }
  save()
  {  
      var that = this;
      this.model.product_id = this.related;
      var disable_updated_box = function() {
        that.globalx.actions.updated = false;
      }
      if (this.globalx.actions.is_new == false)
      {
        this.categoriesx.Update(this.model).subscribe((data:any) => {

          this.globalx.actions.updated = true;
          setTimeout(disable_updated_box, 3000);
        });
      }else
      {
        this.categoriesx.New(this.model).subscribe((data:Data) => {
          this.model = data;   
          this.globalx.actions.is_new = false;   
          this.globalx.actions.updated = true;
          setTimeout(disable_updated_box, 3000);
        });
      }
    
     
    
  }

}
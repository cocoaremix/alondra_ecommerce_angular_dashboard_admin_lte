import { Component, OnInit } from '@angular/core';
import { CdkeysService } from '.././cd_keys.service'
import { Data,Pagination } from '.././cd_keys'
import { Router } from '@angular/router';
import { Globals } from "../../../../../system/components/main/globals";
import { BaseListItemsComponent } from "../../../../../libraries/utils/components/baselist.component";

@Component({
  selector: 'app-cd_keys',
  templateUrl: './cd_keys.component.html',
  styleUrls: ['./cd_keys.component.css']
})
export class CdKeysComponent extends BaseListItemsComponent {

  items: Data[];

   constructor(
      private  routerx: Router, 
      private globalx:Globals,
      private categoriesx:CdkeysService
    ) { 
      super(routerx,globalx,categoriesx);
      this.new_url='/cd_keys/new/';
      this.edit_url ='/cd_keys/edit/';
   }

}

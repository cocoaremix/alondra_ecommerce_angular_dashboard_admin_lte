import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostSelectableComponent } from './post.component';

describe('PostSelectableComponent', () => {
  let component: PostSelectableComponent;
  let fixture: ComponentFixture<PostSelectableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostSelectableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostSelectableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { CategoryService } from '../../categories/category.service'
import { Data,Pagination } from '.././category'
import { Router } from '@angular/router';
import { Globals } from "../../../../../system/components/main/globals";
import { AfterViewInit, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { BaseListItemSelectableComponent } from "../../../../../libraries/utils/components/baseselectable.component";

@Component({
  selector: 'app-categories_selectable',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesSelectableComponent extends BaseListItemSelectableComponent {

  items: Data[];
  @Input() model: Data[];
  @Output() modelChange = new EventEmitter<Data[]>();
  constructor(
      private routerx: Router, 
      private globalx:Globals,
      private categoriesx:CategoryService,
      private elementRefx: ElementRef
    ) {

      super(routerx, globalx,categoriesx, elementRefx);
      this.edit_url = '/categories/edit/';
      this.new_url ='/categories/new';
   }

 
}

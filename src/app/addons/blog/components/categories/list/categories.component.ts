import { Component, OnInit } from '@angular/core';
import { CategoryService } from '../../categories/category.service'
import { Data,Pagination } from '.././category'
import { Router } from '@angular/router';
import { Globals } from "../../../../../system/components/main/globals";
import { BaseListItemsComponent } from "../../../../../libraries/utils/components/baselist.component";

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent extends BaseListItemsComponent  {
    page:number;
    items: Data[];
    query:string;
    selected:boolean;
    pages: number;
    constructor(
      private  routerx: Router, 
      private globalx:Globals,
      private categoriesx:CategoryService
    ) { 
      super(routerx,globalx,categoriesx);
      this.new_url='categories/new/';
      this.edit_url ='categories/edit/';
   }
 
}

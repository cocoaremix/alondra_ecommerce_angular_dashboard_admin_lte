import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, } from '@angular/common/http';
import { Headers } from '@angular/http';
import {RequestOptions, Request, RequestMethod} from '@angular/http';
import { environment } from '../../../../../environments/environment';
import { Globals } from "../../../../system/components/main/globals";
import { CookieService } from 'ngx-cookie-service';
import { HttpJsonHeaderOptionsService } from '../../../../libraries/utils/httpoptions.service';
import { BaseService } from "../../../../libraries/utils/components/base.service";

@Injectable({
  providedIn: 'root'
})
export class PageService  extends BaseService {

   constructor(
    private httpx: HttpClient, 
    private cookiex:CookieService,
    private globalx:Globals, 
    private headerx: HttpJsonHeaderOptionsService) { 
      super(httpx,cookiex, globalx, headerx);
      this.list_url="pages/";
      this.search_url="pages/search/";
      this.new_url="page/";
      this.get_url="page/details/";
      this.find_url="page/find/";
      this.find_slug_url="page/find/slug/";
      this.update_url="page/";
      this.delete_url="page/";
  }
   List(page) {
     
    const postedData = {page:page,post_type:'page'}
    return this.httpx
      .post( 
        environment.url + "pages/",
        postedData,
        this.headerx.get_auth(this.globalx.token)
      );
  }

  Search(page, query) {
    const postedData = {
      page:page,
      query:query,
      post_type:'page'
    }
    return this.httpx
      .post( 
        environment.url + "pages/search/",
        postedData,
        this.headerx.get_auth(this.globalx.token)
      );
  }
}

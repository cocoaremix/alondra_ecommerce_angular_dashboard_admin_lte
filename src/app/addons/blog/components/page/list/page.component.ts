import { Component, OnInit } from '@angular/core';
import { PageService } from '../../page/page.service'
import { Data,Pagination } from '.././page'
import { Router } from '@angular/router';
import { Globals } from "../../../../../system/components/main/globals";
import { BaseListItemsComponent } from "../../../../../libraries/utils/components/baselist.component";

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.css']
})
export class PageComponent extends BaseListItemsComponent  {
    page:number;
    items: Data[];
    query:string;
    selected:boolean;
    pages: number;

    constructor(
      private  routerx: Router, 
      private globalx:Globals,
      private categoriesx:PageService
    ) { 
      super(routerx,globalx,categoriesx);
      this.new_url='pages/new/';
      this.edit_url ='pages/edit/';
     
   }
 
}

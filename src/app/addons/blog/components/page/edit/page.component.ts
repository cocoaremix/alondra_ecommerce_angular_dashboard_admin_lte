import { Component, OnInit } from '@angular/core';
import { Data } from '.././page'
import { PageService } from '../../page/page.service'
import { Router,ActivatedRoute } from '@angular/router';
import { CustomMethodsService } from '../../../../../libraries/utils/custommethods.service';
import { AdviableVar } from '../../../../../libraries/utils/customvars';
import { Globals } from "../../../../../system/components/main/globals";
import { ElementRef,  ViewChild } from '@angular/core';
import { BaseEditComponent } from "../../../../../libraries/utils/components/baseedit.component";

const SimpleMDE: any = require('simplemde');

@Component({
  selector: 'app-page_edit',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.css']
})
export class PageEditComponent extends BaseEditComponent implements OnInit {
  model:Data;
    mde:any;
    @ViewChild('simplemde') textarea: ElementRef
  constructor(  
    private custom_methodsx:CustomMethodsService,
    private routerx: Router,
    private routex:ActivatedRoute, 
    private categoriesx:PageService,
    private globalx:Globals) {
      super(custom_methodsx,routerx,routex,categoriesx,globalx);
      this.list_url = "/pages/"
    }



  ngOnInit() {
    this.globalx.initializeActions();
    this.initialize();
    this.initializeEditor();
    this.routex.params.subscribe(params =>{

      if (typeof params['id'] !== 'undefined') 
      {    
        this.categoriesx.Get(params['id']).subscribe((data:Data) => {
          this.model = data;
          this.mde.codemirror.setValue(this.model.content);
          this.globalx.actions.is_new = false;
        });
      }
      
    });
    
  }

  initialize()
  {
    this.model = {
      title:"",
      meta_title:"",
      meta_description:'',
      slug:"",
      content:'',
      id:null,
      post_type:'page',
      publish:true,
      checked:false,
    };
  }
   setContent(value:string)
  {
    this.model.content = value;
  }
   initializeEditor()
  {
 
    var mde = new SimpleMDE({
      element: this.textarea.nativeElement,
      forceSync: true,
      spellChecker: false,
      status: true
    });
    
     //pass the current instance
    var setValue = this;
    var ChangeEditor = function()
    {

      var value = mde.codemirror.getValue();
    
      setValue.setContent(value) ;  
      
    }
    mde.codemirror.on('change',ChangeEditor);
 
    this.mde = mde;

  }


}

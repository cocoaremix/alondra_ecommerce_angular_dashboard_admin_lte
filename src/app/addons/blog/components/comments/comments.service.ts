import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, } from '@angular/common/http';
import { Headers } from '@angular/http';
import {RequestOptions, Request, RequestMethod} from '@angular/http';
import { environment } from '../../../../../environments/environment';
import { Globals } from "../../../../system/components/main/globals";
import { CookieService } from 'ngx-cookie-service';
import { HttpJsonHeaderOptionsService } from '../../../../libraries/utils/httpoptions.service';
import { BaseService } from "../../../../libraries/utils/components/base.service";

@Injectable({
  providedIn: 'root'
})
export class CommentsService  extends BaseService {

   constructor(
    private httpx: HttpClient, 
    private cookiex:CookieService,
    private globalx:Globals, 
    private headerx: HttpJsonHeaderOptionsService) { 
      super(httpx,cookiex, globalx, headerx);
      this.list_url="comments/";
      this.search_url="comments/search/";
      this.new_url="comment/";
      this.get_url="comment/details/";
      this.find_url="comment/find/";
      this.find_slug_url="comment/find/slug/";
      this.update_url="comment/";
      this.delete_url="comment/";
  }
 
}

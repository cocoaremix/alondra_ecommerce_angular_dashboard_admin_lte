import { Component, OnInit } from '@angular/core';
import { CommentsService } from '../comments/comments.service'
import { Data,Pagination } from './comments'
import { Router } from '@angular/router';
import { Globals } from "../../../../system/components/main/globals";
import { BaseListItemsComponent } from "../../../../libraries/utils/components/baselist.component";

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.css']
})
export class CommentsComponent extends BaseListItemsComponent  {
    page:number;
    items: Data[];
    query:string;
    selected:boolean;
    pages: number;
    constructor(
      private  routerx: Router, 
      private globalx:Globals,
      private categoriesx:CommentsService
    ) { 
      super(routerx,globalx,categoriesx);
      this.new_url='comments/new/';
      this.edit_url ='comments/edit/';
   }
 
}

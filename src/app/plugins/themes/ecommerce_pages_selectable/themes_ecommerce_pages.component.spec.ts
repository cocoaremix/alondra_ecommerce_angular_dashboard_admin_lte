import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThemePagesSelectableComponent } from './themes_ecommerce_pages.component';

describe('ThemePagesSelectableComponent', () => {
  let component: ThemePagesSelectableComponent;
  let fixture: ComponentFixture<ThemePagesSelectableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThemePagesSelectableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThemePagesSelectableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

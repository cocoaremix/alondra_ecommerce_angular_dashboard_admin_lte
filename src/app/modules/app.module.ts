import {enableProdMode} from '@angular/core';
import { ClipboardModule } from 'ngx-clipboard';

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CookieService } from 'ngx-cookie-service';

import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule, HttpClient} from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

/// components
import { PostComponent } from '../addons/blog/components/post/list/post.component';
import { PostEditComponent } from '../addons/blog/components/post/edit/post.component';
import { PostSelectableComponent } from '../addons/blog/components/post/selectable/post.component';

//addons/blog
import { PostService } from '../addons/blog/components/post/post.service';

import { PageComponent } from '../addons/blog/components/page/list/page.component';
import { PageEditComponent } from '../addons/blog/components/page/edit/page.component'
import { PageService } from '../addons/blog/components/page/page.service';

import { CommentsComponent } from '../addons/blog/components/comments/comments.component';
import { CommentsService } from '../addons/blog/components/comments/comments.service';

import { CategoriesComponent } from '../addons/blog/components/categories/list/categories.component';
import { CategoriesEditComponent } from '../addons/blog/components/categories/edit/categories.component';
import { CategoryService } from '../addons/blog/components/categories/category.service';
import { CategoriesSelectableComponent } from '../addons/blog/components/categories/selectable/categories.component';



//addons/ecommerce

import { StatisticsComponent } from '../addons/ecommerce/components/statistics/dashboard/statistics.component';
import { BehaviorsComponent } from '../addons/ecommerce/components/behaviors/list/behaviors.component';
import { UserBehaviorsComponent } from '../addons/ecommerce/components/behaviors/user/behaviors.component';
import { BehaviorsSearchesComponent } from '../addons/ecommerce/components/behaviors/searches/behaviors.component';


import { ArticlesPostComponent } from '../addons/ecommerce/components/post/list/post.component';
import { ArticlesPostEditComponent } from '../addons/ecommerce/components/post/edit/post.component';
import { ArticlesPostSelectableComponent } from '../addons/ecommerce/components/post/selectable/post.component';
import { ArticlesPostSelectableFilesComponent } from '../addons/ecommerce/components/post/selectable_files/files.component';
import { ArticlesPostSelectableMediaComponent } from '../addons/ecommerce/components/post/gallery/gallery.component';


import { ArticlesCategoriesComponent } from '../addons/ecommerce/components/categories/list/categories.component';
import { ArticlesCategoriesEditComponent } from '../addons/ecommerce/components/categories/edit/categories.component';
import { ArticlesCategoriesSelectableComponent } from '../addons/ecommerce/components/categories/selectable/categories.component';
import { ArticlesCategoryService } from '../addons/ecommerce/components/categories/category.service';

import { AttributesComponent } from '../addons/ecommerce/components/attributes/list/attributes.component';
import { AttributesEditComponent } from '../addons/ecommerce/components/attributes/edit/attributes.component';

import { TaxesComponent } from '../addons/ecommerce/components/taxes/list/taxes.component';
import { TaxesEditComponent } from '../addons/ecommerce/components/taxes/edit/taxes.component';

import { DiscountsComponent } from '../addons/ecommerce/components/discounts/list/discounts.component';
import { DiscountsEditComponent } from '../addons/ecommerce/components/discounts/edit/discounts.component';

import { CdKeysComponent } from '../addons/ecommerce/components/cd_keys/list/cd_keys.component';
import { CdkeysEditComponent } from '../addons/ecommerce/components/cd_keys/edit/cd_keys.component';
import { CdkeysSelectableComponent } from '../addons/ecommerce/components/cd_keys/selectable/cd_keys.component';

import { OrdersComponent } from '../addons/ecommerce/components/orders/list/orders.component';
import { OrdersEditComponent } from '../addons/ecommerce/components/orders/edit/orders.component';

//addons/support
import { SupportFaqComponent } from '../addons/support/components/faq/post/list/post.component';
import { SuportFaqEditComponent } from '../addons/support/components/faq/post/edit/post.component';
import { SupporFaqSelectableComponent } from '../addons/support/components/faq/post/selectable/post.component';
import { SupporFaqService } from '../addons/support/components/faq/post/post.service';
import { SupportFaqCategoriesComponent } from '../addons/support/components/faq/categories/list/categories.component';
import { SupportFaqCategoriesEditComponent } from '../addons/support/components/faq/categories/edit/categories.component';
import { FaqCategoryService } from '../addons/support/components/faq/categories/category.service';





import { Globals } from '../system/components/main/globals';
import { AppComponent } from '../system/components/main/app.component';
import { NavigationComponent } from '../system/components/navigation/navigation.component';
import { HeaderComponent } from '../system/components/header/header.component';
import { SidebarComponent } from '../system/components/sidebar/sidebar.component';

import { SigninupComponent } from '../system/components/auth/signinup/signinup.component';

import { LoginComponent } from '../system/components/auth/login/login.component';
import { LoginService } from '../system/components/auth/login/login.service';

import { DashboardComponent } from '../system/components/dashboard/dashboard.component';
import { DashboardService } from '../system/components/dashboard/dashboard.service';


import { MediaComponent } from '../system/components/media/list/media.component';
import { MediaEditComponent } from '../system/components/media/edit/media.component';
import { MediaModalComponent } from '../system/components/media/modal/media.component';
import { MediaModalSelectableComponent } from '../system/components/media/modal_selectable/media.component';
import { MediaService } from '../system/components/media/media.service';


import { TagsComponent } from '../system/components/tags/list/tags.component';
import { TagsEditComponent } from '../system/components/tags/edit/tags.component';
import { TagsSelectableComponent } from '../system/components/tags/selectable/tags.component';
import { TagsService } from '../system/components/tags/tags.service';

import { UsersComponent } from '../system/components/users/list/users.component';
import { UsersEditComponent } from '../system/components/users/edit/users.component';
import { UsersService } from '../system/components/users/users.service';

import { NavigationsComponent } from '../system/components/navigations/list/navigations.component';
import { NavigationsEditComponent } from '../system/components/navigations/edit/navigations.component';
import { NavigationsService } from '../system/components/navigations/navigations.service';

import { SettingsComponent } from '../system/components/settings/settings.component';
import { SettingsService } from '../system/components/settings/settings.service';

import { NavigationsItemsComponent } from '../system/components/navigations_items/list/navigations_items.component';
import { NavigationsItemsEditComponent } from '../system/components/navigations_items/edit/navigations_items.component';
import { NavigationsItemsSelectableComponent } from '../system/components/navigations_items/selectable/navigations_items.component';

import { PermissionComponent } from '../system/components/permissions/permissions/list/permissions.component';
import { PermissionsEditComponent } from '../system/components/permissions/permissions/edit/permissions.component';

import { GroupsComponent } from '../system/components/permissions/groups/list/groups.component';
import { GroupsEditComponent } from '../system/components/permissions/groups/edit/groups.component';

import { UserPermissionComponent } from '../system/components/permissions/user_permissions/list/user_permissions.component';
import { UserPermissionEditComponent } from '../system/components/permissions/user_permissions/edit/user_permissions.component';

import { TranslationsComponent } from '../system/components/translations/list/translations.component';
import { TranslationsEditComponent } from '../system/components/translations/edit/translations.component';

import { ThemesComponent } from '../system/components/themes/themes.component';

import { NotificationsComponent } from '../system/components/notifications/notifications/notifications.component';
import { MessagesComponent } from '../system/components/notifications/messages/messages.component';

//libraries
import { HttpJsonHeaderOptionsService } from '../libraries/utils/httpoptions.service';
import { CustomMethodsService } from '../libraries/utils/custommethods.service';
import { MDEditorComponent } from '../libraries/utils/mdeditor';

///plugins

//ecommerce

import { ThemePagesSelectableComponent } from '../plugins/themes/ecommerce_pages_selectable/themes_ecommerce_pages.component';

//marketplace

import { OrdersMarketplaceComponent } from '../addons/marketplace/components/orders/list/orders.component';
import { UsersMarketplaceComponent } from '../addons/marketplace/components/users/list/users.component';
import { MarketPlaceBehaviorsStatisticsComponent } from '../addons/marketplace/components/statistics/dashboard/statistics.component';

enableProdMode();
export function HttpLoaderFactory(http: HttpClient) 
{
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    ///addons
    //system
    AppComponent,
    HeaderComponent,
    SidebarComponent,
    DashboardComponent,
    NavigationComponent,

    SettingsComponent,

    SigninupComponent,

    LoginComponent,
    //addons/blog
    PostComponent,
    PostEditComponent,
    PostSelectableComponent,
    
    PageComponent,
    PageEditComponent,

    CategoriesComponent,
    CategoriesSelectableComponent,
    CategoriesEditComponent,

    //addons/ecommerce

    StatisticsComponent,
    BehaviorsComponent,
    UserBehaviorsComponent,
    BehaviorsSearchesComponent,
    
    ArticlesPostComponent,
    ArticlesPostEditComponent,
    ArticlesPostSelectableComponent,
    ArticlesPostSelectableFilesComponent,
    ArticlesPostSelectableMediaComponent,
    
    ArticlesCategoriesComponent,
    ArticlesCategoriesEditComponent,
    ArticlesCategoriesSelectableComponent,
    
    AttributesComponent,
    AttributesEditComponent,

    TaxesComponent,
    TaxesEditComponent,

    DiscountsComponent,
    DiscountsEditComponent,

    CdKeysComponent,
    CdkeysEditComponent,
    CdkeysSelectableComponent,

    OrdersComponent,
    OrdersEditComponent,
    //addons/support
    SupportFaqComponent,
    SuportFaqEditComponent,
    SupporFaqSelectableComponent,
    SupportFaqCategoriesComponent,
    SupportFaqCategoriesEditComponent,

    // system 
    MediaComponent,
    MediaEditComponent,
    MediaModalComponent,
    MediaModalSelectableComponent,

    TagsComponent,
    TagsEditComponent,
    TagsSelectableComponent,

    UsersComponent,
    UsersEditComponent,

    NavigationsComponent,
    NavigationsEditComponent,
    
    NavigationsItemsComponent,
    NavigationsItemsEditComponent,
    NavigationsItemsSelectableComponent,

    CommentsComponent,


    PermissionComponent,
    PermissionsEditComponent,

    GroupsComponent,
    GroupsEditComponent,

    TranslationsComponent,
    TranslationsEditComponent,


    UserPermissionComponent,
    UserPermissionEditComponent,

    ThemesComponent,
    NotificationsComponent,
    MessagesComponent,
    ///libraries
    MDEditorComponent,
    ///plugins
    //ecommerce
    ThemePagesSelectableComponent,
    ///

    OrdersMarketplaceComponent,
    UsersMarketplaceComponent,
    MarketPlaceBehaviorsStatisticsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    ClipboardModule,

    TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        })
  ],
  providers: [
      CookieService,
      Globals,
      LoginService,
      DashboardService,
      PostService,
      PageService,
      MediaService,
      TagsService,
      UsersService,
      NavigationsService,
      CommentsService,
      SettingsService,
      CategoryService,
      ArticlesCategoryService,

      //addons/support
      FaqCategoryService,
      SupporFaqService,
      //vendor
      HttpJsonHeaderOptionsService,
      CustomMethodsService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

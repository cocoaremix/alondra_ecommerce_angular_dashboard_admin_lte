import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, } from '@angular/common/http';
import { Headers } from '@angular/http';
import { RequestOptions, Request, RequestMethod} from '@angular/http';
import { environment } from '../../../../environments/environment';
import { Globals } from "../main/globals";
import { CookieService } from 'ngx-cookie-service';
import { HttpJsonHeaderOptionsService } from '../../../libraries/utils/httpoptions.service';
import { Data,Pagination } from './settings';

@Injectable({
  providedIn: 'root'
})
export class SettingsService {

  
  constructor(
    private http: HttpClient, 
    private cookie:CookieService,
    private global:Globals, 
    private header: HttpJsonHeaderOptionsService) { }
  Get() {
  
    return this.http
      .get( 
        environment.url + "settings/",
        this.header.get_auth(this.global.token)
      );
  }

  Update(postedData) {
   
    return this.http
      .put( 
        environment.url + "settings/",
        postedData,
        this.header.get_auth(this.global.token)
      );
  }

}

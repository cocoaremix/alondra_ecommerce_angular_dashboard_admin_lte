import { Component, OnInit } from '@angular/core';
import { Data,Role,ChangePassword } from '.././users';
import { UsersService } from '../../users/users.service';
import { Router,ActivatedRoute } from '@angular/router';
import { CustomMethodsService } from '../../../../libraries/utils/custommethods.service';
import { AdviableVar } from '../../../../libraries/utils/customvars';
import { Globals } from "../../main/globals";

@Component({
  selector: 'app-users_edit',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersEditComponent implements OnInit {
  model:Data;
  model2:ChangePassword;
  passwordchanged:boolean;
  roles:Role[];
  constructor(  
    private custom_methods:CustomMethodsService,
    private router: Router,
    private route:ActivatedRoute, 
    private categories:UsersService,
    private global:Globals) { }


  ngOnInit() {
      this.global.initializeActions();
      this.initialize();
      this.route.params.subscribe(params =>{

        if (typeof params['id'] !== 'undefined') 
        {       
          
          this.categories.Get(params['id']).subscribe((data:Data) => {
          this.model = data;
          this.global.actions.is_new = false;
        });
        }else{
          if(this.router.url == "/profile")
          {
            this.categories.me().subscribe((data:Data) => {
              this.model = data;
              this.global.actions.is_new = false;
            });
          }
        }
         
        
        
      });
    
  }
  initialize()
  {  
    this.passwordchanged = false;
    this.roles = [
   
    
      {
        id:"autor",
        name:"USER_ROLE_AUTOR_LABEL",
      },
      {
        id:"editor",
        name:"USER_ROLE_EDITOR_LABEL",
      },
      {
        id:"user",
        name:"USER_ROLE_USER_LABEL",
      },
    
    ]
    this.model2 =  {
      id:null,
      old_password: "",
      new_password: "",
      repeat_new_password: ""
    };
    this.model =  {
      id:null,
      role:"",
      first_name:"",
      last_name:"",
      is_active:true,
      is_superuser:false,
      username:"",
      email:"",
      password:"",
      nick:"",
      checked:false,
      parnetship:false,
      parnetship_requested:false,

    };
  }
  save()
  {  
      var that = this;
      var disable_updated_box = function() {
        that.global.actions.updated = false;
      }
      if (this.global.actions.is_new == false)
      {
        this.categories.Update(this.model).subscribe((data:any) => {

          this.global.actions.updated = true;
          setTimeout(disable_updated_box, 3000);
        });
      }else
      {
        this.categories.New(this.model).subscribe((data:Data) => {
          this.model = data;   
          this.global.actions.is_new = false;   
          this.global.actions.updated = true;
          setTimeout(disable_updated_box, 3000);
        });
      }
    
     
    
  }

  updatePassword()
  {  
      var that = this;
      this.model2.id = this.model.id;
      var disable_updated_box = function() {
        that.passwordchanged = false;
      }
      if (this.global.actions.is_new == false)
      {
        this.categories.UpdateMePassword(this.model2).subscribe((data:any) => {

          this.passwordchanged = true;
          setTimeout(disable_updated_box, 3000);
        });
      }
    
     
    
  }
 
  back()
  {
     this.router.navigate(['/users' ]);
  }
  DELETE()
  {
   
    this.categories.Delete(this.model.id).subscribe((data:any) => {
      this.router.navigate(['/users' ]);

    });
    
  }

}

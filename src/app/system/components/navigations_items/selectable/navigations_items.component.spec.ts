import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavigationsItemsSelectableComponent } from './navigations_items.component';

describe('NavigationsEditComponent', () => {
  let component: NavigationsItemsSelectableComponent;
  let fixture: ComponentFixture<NavigationsItemsSelectableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavigationsItemsSelectableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavigationsItemsSelectableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

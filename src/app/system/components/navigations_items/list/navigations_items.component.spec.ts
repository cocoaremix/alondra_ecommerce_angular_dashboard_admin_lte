import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavigationsItemsComponent } from './navigations_items.component';

describe('NavigationsItemsComponent', () => {
  let component: NavigationsItemsComponent;
  let fixture: ComponentFixture<NavigationsItemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavigationsItemsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavigationsItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

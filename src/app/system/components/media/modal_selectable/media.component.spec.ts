import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MediaModalSelectableComponent } from './media.component';

describe('MediaModalSelectableComponent', () => {
  let component: MediaModalSelectableComponent;
  let fixture: ComponentFixture<MediaModalSelectableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MediaModalSelectableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MediaModalSelectableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

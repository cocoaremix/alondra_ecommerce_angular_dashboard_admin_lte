import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import { Globals } from "../../main/globals";
import { MediaService } from ".././media.service";
import { Data,Pagination } from ".././media";
declare var jQuery:any;
declare var $:any;
declare var Clipboard:any;

import {Directive,ElementRef,Input,Output,EventEmitter} from '@angular/core';;



@Component({
  selector: 'app-media',
  templateUrl: './media.component.html',
  styleUrls: ['./media.component.css']
})
export class MediaComponent implements OnInit {
  	search_pressed:boolean;
  	basepath:string;
  	progresss:number;
  	progress:string;
  	query:string;
  	parent:boolean;
  	items = []
  	current:string;
  	folder_path:string;
  	page:number;
  	newname:string;
	isnew:false;
 	constructor(
    	private router: Router, 
    	private global:Globals,
    	private categories:MediaService
    ) { }

     slugify(text)
		{
		  return text.toString().toLowerCase()
		    .replace(/\s+/g, '-')           // Replace spaces with -
		    .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
		    .replace(/\-\-+/g, '-')         // Replace multiple - with single -
		    .replace(/^-+/, '')             // Trim - from start of text
		    .replace(/-+$/, '');            // Trim - from end of text
		}

	ngOnInit() 
	{
	  //inject dashboard

	  	this.search_pressed = false;
		this.basepath = "/";
		this.current = "";
		this.folder_path = "/";
		this.page = 1;
		this.query = "";
		this.items = [];
		this.newname = ""
		this.isnew= false;
		this.parent = false;
		this.progresss = 100;
		this.progress = this.progresss+"%";

		this.load_items ();
		this.initializeEditor();
    
	}

	load_items ()
	{
		this.items = [];
		this.categories.ListFolder(this.folder_path).subscribe(
			(data:Data[]) => 
		{
	      	data.forEach(obj => {
	            obj.checked = false;
	            obj.selected = false;
	            obj.editable = false;
	            obj.duplicate = false;
	            obj.oldname = obj.name;
	            obj.folder = true;
	        });
	       	this.items.push(...data)
	   		this.categories.List(this.folder_path).subscribe(
			(data:Data[]) => 
			{
		      	data.forEach(obj => {
		            obj.checked = false;
		            obj.selected = false;
		            obj.oldname = obj.name;
		            obj.duplicate = false;
		            obj.editable = false;
		            obj.folder = false;
		        });
		       this.items.push(...data)
	       
	    	});
	    });

	}


	newItem = function()
	{
		if(this.newname.length > 0)
		{
			this.newname=this.slugify(this.newname);
			if(this.folder_path != "/" && this.folder_path.length > 0)
			{
				var itemx = this.folder_path.match( /^\/{1}/ );
				var itemxx = this.folder_path.match( /\/{1}$/ );
				var start = "";
				var end = "";
				if(itemx==null)
				{	
					start = "/";
				}
				if(this.current.length > 0 && itemxx==null)
				{	
					end = "/";
				}
				this.folder_path = start+this.folder_path +end;
				
			}else{

				this.folder_path = "/";
			}
			this.categories.SaveDir( 
				this.folder_path,
				this.folder_path + this.newname,
				this.newname
			).subscribe((data:any) => {
		        if(data.success == true)
				{
					this.newname = "";
					this.load_items()
				}
			});
		}
		
	};	

	SearchPressed()
	{

		if(this.query.length == 0)
		{
			this.search_pressed = false;
		}else
		{
			this.search_pressed = true;
		}	

	}

	clearSearch()
	{
		this.query = null;
		this.search_pressed = false;
	}
	Editable(item:Data)
	{
		if (item.editable == false)
		{
			item.editable = true;
			
		}
	}
	Changename(item:Data)
	{
		if (item.editable == true)
		{
			item.editable = false;
			if(item.duplicate == false)
			{
				item.editable = false;
				if(item.oldname !== item.name)
				{
		 			if(item.folder == true)
			        {

			        	this.categories.UpdateDir(
			        		this.folder_path+ item.oldname,
			        		this.folder_path + item.name).subscribe((data:any) => {

			        		if(data.success == true)
							{
								item.oldname = item.name;
							}
						});
			        	
			        }
				}
			}
		}
	}
	NAVIGATE(item:Data)
	{
		if (item.folder == true)
		{
			this.current = item.name + "/";
			this.parent = true;
			var itemx = this.folder_path.match( /^\/{1}/ );
			
			var start = "";
			var itemxxx = this.folder_path.match( /^\/{1}/ );

			if(itemxxx==null)
			{	
				start = "/";
			}
			
			
			this.folder_path = start+this.folder_path.concat(this.current );
		

			//$scope.makeTodos();
			this.load_items()
		}
	}

	SEARCH_FOLDER()
	{
		if(this.folder_path != "/" && this.folder_path.length > 0)
		{
			var itemx = this.folder_path.match( /^\/{1}/ );
			var itemxx = this.folder_path.match( /\/{1}$/ );
			var start = "";
			var end = "";
			if(itemx==null)
			{	
				start = "/";
			}
			if(this.current.length > 0 && itemxx==null)
			{	
				end = "/";
			}
			this.folder_path = start+this.folder_path +end;
			this.load_items()
		}else{

			this.folder_path = "/";
		}
		
	}
	NAVIGATE_PARENT ()
	{
		
			if(this.folder_path != "/" && this.folder_path.length > 0)
		{

			var thispath = this.folder_path.slice(0, -this.current.length);
			var end = ""
			var start = "";
			var itemx = thispath.match( /((\w+)\/)+/ );
			var itemxx = thispath.match( /\/{1}$/ );

			var itemxxx = thispath.match( /^\/{1}/ );
			if(this.current.length == 0 &&  itemxxx==null)
			{	
				start = "/";
			}
			if(itemx!=null)
			{	
				this.parent = true
				this.current = itemx[1];

			}else{
				this.parent = false;
				this.current = "/";
			}
			
			if(this.parent == true && this.current.length > 0 && itemxx==null)
			{	
				end = "/";
			}

			this.folder_path = start+thispath+end ;
			if(this.folder_path.length == 0)
			{
				this.folder_path = "/";
			}
			this.load_items()
			
		}else{
			
			this.folder_path = "/";
		}
		
		
	}


		DELETE (itemx:Data)
		{
			
	        if(itemx.folder == true)
	        {
	        	this.categories.DeleteDir(this.folder_path + itemx.name)
	        		.subscribe((data:any) => {

			        if(data.success == true)
					{
						this.items = this.items.filter(obj => obj.pk != itemx.pk)	
					}
				});
	        }else
	        {
				this.categories.DeleteFile(
					this.folder_path,
					this.folder_path + itemx.name
				).subscribe((data:any) => {

			        if(data.success == true)
					{
						this.items = this.items.filter(obj => obj.pk != itemx.pk)	
					}
				});


				
	        }
			
		}

  Upload(files:FileList[]) {
      var fd = new FormData();

      fd.append("query", this.folder_path  );
      fd.append("newname", this.folder_path  );

      jQuery.each(files,function(i,val){
        fd.append("file",val)
      })

   
			        	
      this.categories.UpdateFile(fd).subscribe( (event: any) => {
      		jQuery('#id_files').val(null)
         this.load_items()
       
      });
      
  };
  initializeEditor()
  { 
    /*
      pending create ajax with angular progressbar ajax put
    */       

    this.progresss = 0;
    var that = this;
   
    var open_uploader = function (e)
    {  
      e.preventDefault();
      e.stopPropagation();
      jQuery('#id_files').focus().trigger('click');
    }

    jQuery("#holder").on('click',open_uploader);
    var clicks = function(e)
    {
      e.stopPropagation();
    }
    jQuery('#id_files').on('click',clicks);
    
    jQuery('#id_files').bind('change', function (e) {
      e.stopPropagation();
      var files:FileList[] = e.target.files;
      that.Upload(files);
    });

    jQuery("#holder").on('drop', function(e) {
      e.preventDefault();
      e.stopPropagation();
      
      if (e.dataTransfer){
          if (e.dataTransfer.files.length > 0) {
             var files:FileList[] = e.dataTransfer.files;
              that.Upload(files);
          }
      }
      return false;
    });
    jQuery("#holder").on('dragover', function(e) {
        e.preventDefault();
        e.stopPropagation();
    });
    jQuery("#holder").on('dragenter', function(e) {
        e.preventDefault();
        e.stopPropagation();
    });

  }
}

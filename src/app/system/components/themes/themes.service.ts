import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, } from '@angular/common/http';
import { Headers } from '@angular/http';
import {RequestOptions, Request, RequestMethod} from '@angular/http';
import { environment } from '../../../../environments/environment';
import { Globals } from "../main/globals";
import { CookieService } from 'ngx-cookie-service';
import { HttpJsonHeaderOptionsService } from '../../../libraries/utils/httpoptions.service';
import { Data,Pagination } from './themes';

@Injectable({
  providedIn: 'root'
})
export class ThemesService {

  uploadprogress: number;
  constructor(
    private http: HttpClient, 
    private cookie:CookieService,
    private global:Globals, 
    private header: HttpJsonHeaderOptionsService) {
    
      this.uploadprogress = 0;
    
    }

  List(page) {
     
    const postedData = {page:page}
    return this.http
      .post( 
        environment.url + "settings/themes/list/",
        postedData,
        this.header.get_auth(this.global.token)
      );
  }

  New(postedData) {
   
    return this.http
      .put( 
        environment.url + "settings/themes/upload/",
        postedData,
        this.header.get_auth(this.global.token)
      );
  }


  Get() {
 
    return this.http
      .get( 
        environment.url + "settings/",
        this.header.get_auth(this.global.token)
      );
  }
  Find(name) {
    
    const postedData = {
      name:name
    }    
    return this.http
      .post( 
        environment.url + "settings/themes/find/",
        postedData,
        this.header.get_auth(this.global.token)
      );
  }
  Search(page, query) {
    const postedData = {
      page:page,
      query:query
    }
    return this.http
      .post( 
        environment.url + "settings/themes/search/",
        postedData,
        this.header.get_auth(this.global.token)
      );
  }

  Update(postedData) {
   
    return this.http
      .put( 
        environment.url + "settings",
        postedData,
        this.header.get_auth(this.global.token)
      );
  }

  Upload(postedData) {
   
    return this.http
      .put(
        environment.url + "settings/themes/upload/",
        postedData,
        this.header.get_auth_params_option_upload(this.global.token),

      );
  }

  Delete(id:string) {

    const postedData = {
      id:id
    }  
    return this.http
      .delete( 
        environment.url + "settings/themes/delete/",
        this.header.get_auth_params_option(this.global.token,postedData)
      );
     
  }


  UpdateThemePage(postedData) {
   
    return this.http
      .put( 
        environment.url + "settings/themes/page/change/",
        postedData,
        this.header.get_auth(this.global.token)
      );
  }
  GetThemePage(parent,modulex) {
    const postedData = {
      parent:parent,
      module:modulex,
    }
    return this.http
      .post( 
        environment.url + "settings/themes/page/get/",
        postedData,
        this.header.get_auth(this.global.token)
      );
  }


}

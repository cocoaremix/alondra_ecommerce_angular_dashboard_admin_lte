
export interface LangCode {
	id: string;
	name:string;

}


export interface Data {
	id: number;
	publish:boolean;
	key:string;
	lang_code:string;
	content:string;
	checked: boolean;
	selected:boolean;
}

export interface Pagination {
	pages: number;
	items: Data[];
	next_page: string;
	prev_page: string;
}
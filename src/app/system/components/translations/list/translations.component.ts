import { Component, OnInit } from '@angular/core';
import { TranslationsService } from '../../translations/translations.service'
import { Data,Pagination } from '.././translations'
import { Router } from '@angular/router';
import { Globals } from "../../main/globals";
import { BaseListItemsComponent } from "../../../../libraries/utils/components/baselist.component";

@Component({
  selector: 'app-translations',
  templateUrl: './translations.component.html',
  styleUrls: ['./translations.component.css']
})
export class TranslationsComponent extends BaseListItemsComponent {
  page:number;
  items: Data[];
  query:string;
  selected:boolean;
  pages: number;
   constructor(
      private  routerx: Router, 
      private globalx:Globals,
      private categoriesx:TranslationsService
    ) { 
      super(routerx,globalx,categoriesx);
      this.new_url='/translations/new/';
      this.edit_url ='/translations/edit/';
   }
 
}


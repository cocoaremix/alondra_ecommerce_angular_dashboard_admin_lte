
import { Data as Groups } from '../groups/./groups'

export interface Data {
	id: number;
	userid:number;
	groups_lists: Groups[];
	checked: boolean;
	selected: boolean;
}
export interface Role {
	id: string;
	name:string;
}

export interface Pagination {
	pages: number;
	items: Data[];
	next_page: string;
	prev_page: string;
}
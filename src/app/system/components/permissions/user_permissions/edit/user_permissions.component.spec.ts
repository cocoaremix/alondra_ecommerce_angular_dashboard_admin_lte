import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserPermissionEditComponent } from './user_permissions.component';

describe('UserPermissionEditComponent', () => {
  let component: UserPermissionEditComponent;
  let fixture: ComponentFixture<UserPermissionEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserPermissionEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserPermissionEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

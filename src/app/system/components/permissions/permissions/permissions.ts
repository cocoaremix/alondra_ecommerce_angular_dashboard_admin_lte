


export interface Data {
	id: number;
	module: string;
	action: string;
	checked: boolean;
	selected: boolean;
}
export interface Role {
	id: string;
	name:string;
}
export interface Permissions {
	id: string;
	name:string;
}


export interface Pagination {
	pages: number;
	items: Data[];
	next_page: string;
	prev_page: string;
}

import { Data as Permisions } from '../permissions/./permissions'

export interface Data {
	id: number;
	name: string;
	permission_lists: Permisions[];
	checked: boolean;
	selected: boolean;
}
export interface Role {
	id: string;
	name:string;
}

export interface Pagination {
	pages: number;
	items: Data[];
	next_page: string;
	prev_page: string;
}
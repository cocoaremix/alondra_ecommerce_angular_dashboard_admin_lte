export interface Data {
	id: number;
	first_name: string,
	last_name: string,
	username: string,
	email: string,
	nick: string,
	password:string;
}

export interface Actvation {
	activation:string,
	email: string,
	new_password:string,
	repeat_new_password:string;
}

export interface Reset {
	email: string;
}

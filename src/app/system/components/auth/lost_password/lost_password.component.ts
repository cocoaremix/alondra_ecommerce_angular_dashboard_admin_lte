import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { LoginService } from '../login.service';
import { Globals } from "../../main/globals";
import { Actvation } from '../users';

@Component({
  selector: 'app-signinup',
  templateUrl: './signinup.component.html',
  styleUrls: ['./signinup.component.css']
})
export class LostPasswordComponent implements OnInit {
  model:Actvation;
  constructor( 
    private router: Router,   
    private categories: LoginService,
    private global:Globals
  ) 
  {
    this.initialize();
  }


  ngOnInit() {}
  Loginx()
  {
    this.router.navigate(['/login']);
  }
  Save()
  {
    var that = this;
    var disable_updated_box = function() {
      that.global.actions.updated = false;
    }
    if (this.global.actions.is_new == false)
    {
      this.categories.New(this.model).subscribe((data:any) => {

        this.global.actions.updated = true;
        setTimeout(disable_updated_box, 3000);
      });
    }
    this.router.navigate(['/login']);
  }

  initialize()
  {  
   
    this.model =  {
      activation: "",
      email: "",
      new_password: "",
      repeat_new_password: ""
    };
  }


}

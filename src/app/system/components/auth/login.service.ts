import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { CookieService } from 'ngx-cookie-service';
import { HttpJsonHeaderOptionsService } from '../../../libraries/utils/httpoptions.service';
import { Globals } from "../../../system/components/main/globals";

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  
    constructor(
    private http: HttpClient, 
    private cookie:CookieService,
    private global:Globals, 
    private header: HttpJsonHeaderOptionsService) { }

   New(postedData) {
   
    return this.http
      .post( 
        environment.url + "new/user/",
        postedData,
        this.header.get_auth(this.global.token)
      );
  }
  Reset(postedData) {
   
    return this.http
      .post( 
        environment.url + "user/reset/",
        postedData,
        this.header.get_auth(this.global.token)
      );
  }

  Activate(postedData) {
   
    return this.http
      .post( 
        environment.url + "user/reset/activate/",
        postedData,
        this.header.get_auth(this.global.token)
      );
  }
  getUser(username, password) {
    const postedData = { 
      username: username, 
      password: password
    };
    return this
            .http
            .post( 
              environment.url + "get_auth_token/",
              postedData,
              httpOptions
            );
        }
}
